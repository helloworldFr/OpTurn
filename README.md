# Solver for Time dependents vehicle routing problem with time windows (OpTurn 0.1)

This project is a student project which was develop by 4 students as proof of concept.

This is an TDVRPTW which use insertion method by Solomon (1987) to create
a pool of solutions and use Large neighborhood search to optimise the cost.

To take car of the traffic part of the problem we use Google Maps API to get prediction duration
of a trip.

## Constraints
1. Appointment have
    + Skill (0 | 1 | 2)
    + Fixed duration
    + Time Windows (cannot be done outside the time windows)
2. Every appointment have to be filled
3. Technician have
    + Skill (0 | 1 | 2)
    + Time Windows (cannot start/finish his working day outside the time windows)
4. Lunch break have to be done at the corresponding time (No trip must be done in this break so it had to be done before or after)

## Objective Function
The function objective take care of :
+ The fixed cost of the technician u
+ The hour cost h ($/h)
+ The km cost k (€/km)

```of = Number_of_tech * u + total_number_of_hour_work + total_number_km * k```

## Workflow
1. Load instance
2. Create pool of solutions with Solomon insertion method
3. Run LNS on the best (minimal cost) solution

## How to use it
### Requirements
+ Java 1.7
+ Maven 3

### Unit
Every distance are in kilometer and every time in minutes

### Config file
To use the project you have to set parameters in the config.xml :
```XML
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
<properties>
    <comment></comment>
    <entry key="GOOGLE_MAP_API_KEY">{YOUR_API_TOKEN}</entry>
    <entry key="STEP">{STEP_TIME_BETWEEN_REQUEST}</entry>

    <entry key="HEURISTIC_ITERATION">{NUMBER_OF_SOLUTIONS_IN_POOL}</entry>
    <entry key="LNS_ITERATION">{NUMBER_OF_ITERATIONS_IN_LNS}</entry>
    <entry key="LNS_MAX_DESTROY">{MAXIMUN_NUMBER_OF_APPOINTMENT_LNS_CAN_REMOVE}</entry>
</properties>
```
> TIPS : the step define the interval you need between two request for the same trip like : 8:00 and 9:30 for STEP = 30

### Inputs (JSON/RL)
You can use to kind of file as input (instance)

1. The first one is proprietary file .rl : (You have to respect exactly the syntax)
```
    #technician_cost
    20
    #cout_kilometrique
    0.1
    #technician_hourly_cost
    0.1
    #number_of_technicians
    1
    #technicians
    600 720 780 1140 3

    #number_appointments
    3
    #appointments
    1 780 1020 15 1 0.0 0.0
    4 480 1020 15 1 0.0 0.0
    8 900 1020 12 1 0.0 0.0

    #distances
        0	1	4	8
    0	0.0	4.11	3.222	1.495
    1	4.429	0.0	2.236	3.055
    4	3.943	2.243	0.0	2.048
    8	2.27	2.979	2.091	0.0
    #durations
        0	1	4	8
    0	0.0	10.0166666667	7.38333333333	4.01666666667
    1	10.0833333333	0.0	5.95	7.51666666667
    4	8.55	5.98333333333	0.0	5.28333333333
    8	4.76666666667	7.05	4.41666666667	0.0
    ```

> Distances and Duration are optional if you use Google Maps but you need to give for each the first line to the depot

2. The second one is a JSON file :
```JSON
{
  "edges": [
    [
      [
        {
          "distance": 0.0,
          "time": 0
        },
        ...
      ],
      ...
    ],
    ...
  ],
  "edgesDepot": [
    {
      "distance": 4.11,
      "time": 10
    },
    ...
  ],
  "appointmentManager": {
    "appointments": [
      {
        "id": 1,
        "nodeId": 0,
        "startTime": 780,
        "endTime": 1020,
        "skill": "SKILL_0",
        "durationTime": 15
      },
      ...
    ]
  },
  "constantManager": {
    "kilometre": 0.1,
    "technician": 20.0,
    "hourlyFee": 0.1,
    "minuteStart": 0,
    "minuteEnd": 0,
    "stepMinute": 30,
    "depotId": 4
  },
  "geographicalNodeManager": {
    "currentId": 3,
    "nodes": [
      {
        "id": 0,
        "longitude": 0.0,
        "latitude": 1.0
      },
      ...
    ]
  },
  "technicianManager": {
    "technicians": [
      {
        "startTimeMorning": 600,
        "endTimeMorning": 720,
        "startTimeAfternoon": 780,
        "endTimeAfternoon": 1140,
        "skill": "SKILL_2"
      }
    ]
  },
  "timeIntervalManager": {
    "timeIntervals": [
      {
        "id": 0,
        "startTime": 600,
        "endTime": 630
      },
      ...
    ],
    "currentId": 18,
    "startTime": 600,
    "step": 30
  }
}
```

> TIPS : Tab of edges First dimension is time (sort by time interval ID), the two other are source and destination nodes

> Distances and Duration are optional if you use Google Maps but you need to give for each the first line to the depot

### Quick start
Run with maven the next command in the project folder to download all dependencies
```
mvn clean install compile
```
#### Data from google
If you want use the traffic Google Maps make sure you set the [config.xml](#config-file) and the API TOKEN

(You have to run this command in target folder)
```
java -cp . fr.univ_tours.polytech.di4.project.starter.OpTurnGoogle {INSTANCE.RL/.JSON} {SAVE_FILE.TXT}
```
> SAV_FILE.TXT is optional

This will produce solution in json file and write cost in the console.
#### Data from RL
If you already have data in your instance file (RL) so you can run the algorithm without traffic to do, put your files (you can have more than one) in the instances directory
and run :
```
java -cp . fr.univ_tours.polytech.di4.project.starter.OpTurnRL
```
This will produce solution file for each instance file in the same folder

### Outputs
The general output is save in the solution file on JSON format but you can get resume information from the console log like the cost of the solution

***

Implementation by Mahtieu Archat, Théo Gilbert, Théo Ménard, Timothy Nibeaudeau, Copyright (c) 2016-2017
