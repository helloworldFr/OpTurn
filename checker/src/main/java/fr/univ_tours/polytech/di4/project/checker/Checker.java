package fr.univ_tours.polytech.di4.project.checker;


import fr.univ_tours.polytech.di4.project.algorithm.solution.Route;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;
import fr.univ_tours.polytech.di4.project.data.appointment.AppointmentManager;
import fr.univ_tours.polytech.di4.project.data.instance.InstanceDistanceMatrix;
import fr.univ_tours.polytech.di4.project.data.technician.Technician;
import fr.univ_tours.polytech.di4.project.data.technician.TechnicianManager;
import fr.univ_tours.polytech.di4.project.data.technician.TechnicianSkills;

/**
 * @author Archat Mathieu
 * @version 0.1
 *          This class store an object which contain coordinates of each node
 */
public class Checker {

    private AppointmentManager appointmentManager;
    private TechnicianManager technicianManager;

    private Route[] solution;
    private InstanceDistanceMatrix distance;


    /**
     * Constructor of Checker
     *
     * @param solution
     * @param distance distance of each node
     */
    public Checker(Route[] solution, InstanceDistanceMatrix distance) {
        this.appointmentManager = distance.getAppointmentManager();
        this.technicianManager = distance.getTechnicianManager();
        this.solution = solution;
        this.distance = distance;
    }

    /**
     * Check le solution
     *
     * @return true if solution is correct and false when the solution isn't correct
     */
    public boolean checkSolution() {

        if (!checkNumberOfTechnician())
            return false;

        if (!checkAllAppointmentIsCovered())
            return false;


        int sizeListOfAppointment = solution.length;
        for (int indexAppointment = 0; indexAppointment < sizeListOfAppointment; indexAppointment++) {

            int sizeNode = solution[indexAppointment].size();
            if (sizeNode > 0) {
                for (int indexNode = 0; indexNode < sizeNode; indexNode++) {
                    if (!checkSkill(solution[indexAppointment].get(indexNode), solution[indexAppointment].getTechnician()))
                        return false;
                }

                if (!checkDuration(indexAppointment, solution[indexAppointment].getTechnician()))
                    return false;

                if (!checkDepotAndMeal())
                    return false;
            }
        }

        // all is right
        return true;
    }


    /**
     * Check the number of technician used
     *
     * @return true if number of technician does not exceed the initial number
     */
    private boolean checkNumberOfTechnician() {
        int nbTech = technicianManager.getTechnicians().size();
        int nbTechSolution = solution.length;

        return nbTech >= nbTechSolution;
    }

    /**
     * Check if the technician had skill needed for appointment
     *
     * @param appointment the target appointment
     * @param technician  the technician who made the appointment
     * @return true if the technician had skill, false otherwise
     */
    private boolean checkSkill(Appointment appointment, Technician technician) {
        //if technician had the ability
        if (technician.getSkill() != TechnicianSkills.SKILL_2) // if the technician can not do everything
        {
            if (appointment.getSkill() != technician.getSkill())
                return false;
        }
        return true;

    }

    /**
     * Check if the duration of all appointment doesn't exceeds work hour of technician
     *
     * @param index      of 'route'
     * @param technician to acces the techicians
     * @return true, if the technician can make all appointment in his work hour without exceeds, false otherwise
     */
    private boolean checkDuration(int index, Technician technician) {

        Route route = solution[index];
        //First visit starts before the starting time of the technician
        if (technician.getStartTimeMorning() > route.getAppointmentStartTime(0))
            return false;

        // total work time
        int TotalTime = route.getAppointmentStartTime(0);
        boolean startsMorning = route.getAppointmentStartTime(0) < technician.getStartTimeAfternoon();

        if (route.size() >= 2)
            TotalTime += distance.getAllTimeIntervalTravelingInformationMatrix(route.get(0).getNodeId(), route.get(1).getNodeId())[0].getTime();

        for (int i = 1; i < route.size() - 1; i++) {
            TotalTime += route.get(i).getDurationTime();
            TotalTime += distance.getTravelingInformation(route.get(i).getNodeId(), route.get(i + 1).getNodeId(), route.get(i).getEndTime()).getTime();
        }
        TotalTime += route.get(route.size() - 1).getDurationTime();

        if (startsMorning && TotalTime >= technician.getStartTimeAfternoon())
            TotalTime += technician.getStartTimeAfternoon() - technician.getEndTimeMorning();

        //Last visit end after the ending time of the technician
        if (TotalTime > technician.getEndTimeAfternoon()) {
            return false;
        }

        return true;
    }


    /**
     * Check if the technician had in first and last appointment, the depot, and if he had a meal time
     *
     * @return true if he had, false otherwise
     */
    private boolean checkDepotAndMeal() {
        //TODO
        return true;
    }


    /**
     * Check if all appointment is coverd
     *
     * @return true if ti is, false otherwise
     */
    private boolean checkAllAppointmentIsCovered() {

        int nbRDV = appointmentManager.getAppointments().size();
        int nbRDVSolution = 0;

        for (Route aSolution : solution) {

            nbRDVSolution += aSolution.size();

        }

        return nbRDV == nbRDVSolution;
    }
}
