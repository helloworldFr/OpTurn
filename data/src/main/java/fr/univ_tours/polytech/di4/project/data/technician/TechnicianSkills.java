package fr.univ_tours.polytech.di4.project.data.technician;

/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This enum store all possible technician skill
 */
public enum TechnicianSkills {
    SKILL_0, SKILL_1, SKILL_2
}
