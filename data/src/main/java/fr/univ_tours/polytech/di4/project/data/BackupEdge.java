package fr.univ_tours.polytech.di4.project.data;

/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This class represent a backup edge to store request
 */
public class BackupEdge {
    private String source;
    private String destination;
    private String time;
    private double distance;
    private int duration;

    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }

    public String getTime() {
        return time;
    }

    public double getDistance() {
        return distance;
    }

    public int getDuration() {
        return duration;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BackupEdge edge = (BackupEdge) o;

        if (!source.equals(edge.source)) return false;
        if (!destination.equals(edge.destination)) return false;
        return time.equals(edge.time);

    }

    @Override
    public int hashCode() {
        int result = source.hashCode();
        result = 31 * result + destination.hashCode();
        result = 31 * result + time.hashCode();
        return result;
    }
}
