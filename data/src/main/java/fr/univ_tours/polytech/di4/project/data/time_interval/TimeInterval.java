package fr.univ_tours.polytech.di4.project.data.time_interval;

import java.io.Serializable;

/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This class represent a time interval and an unique ID
 */
public class TimeInterval implements Serializable {
    private int id;
    private int startTime;
    private int endTime;

    /**
     * Constructor of TimeInterval
     *
     * @param id        unique id of the timeInterval
     * @param startTime of the interval
     * @param endTime   of the interval
     */
    TimeInterval(int id, int startTime, int endTime) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public int getId() {
        return id;
    }

    public int getStartTime() {
        return startTime;
    }

    public int getEndTime() {
        return endTime;
    }

}
