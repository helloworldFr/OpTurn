package fr.univ_tours.polytech.di4.project.data.instance;

import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;
import fr.univ_tours.polytech.di4.project.data.appointment.AppointmentManager;
import fr.univ_tours.polytech.di4.project.data.cost.ConstantManager;
import fr.univ_tours.polytech.di4.project.data.geographical.GeographicalNode;
import fr.univ_tours.polytech.di4.project.data.geographical.GeographicalNodeManager;
import fr.univ_tours.polytech.di4.project.data.technician.TechnicianManager;
import fr.univ_tours.polytech.di4.project.data.time_interval.TimeInterval;
import fr.univ_tours.polytech.di4.project.data.time_interval.TimeIntervalManager;

import java.io.Serializable;
import java.util.List;


/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This class store the 3 dimensional matrix which be use in the algorithm
 */
public class InstanceDistanceMatrix implements Serializable {
    private EdgeTravelingInformation[][][] edges;
    private EdgeTravelingInformation[] edgesDepot;

    private AppointmentManager appointmentManager;
    private ConstantManager constantManager;
    private GeographicalNodeManager geographicalNodeManager;
    private TechnicianManager technicianManager;
    private TimeIntervalManager timeIntervalManager;

    /**
     * Create an instance from all manager
     *
     * @param appointmentManager      manager which handle appointment
     * @param constantManager         manager which handle all constant
     * @param geographicalNodeManager manager which handle all nodes
     * @param technicianManager       manager which handle all technician informations
     * @param timeIntervalManager     manager which handle all timeInterval
     */
    public InstanceDistanceMatrix(AppointmentManager appointmentManager, ConstantManager constantManager, GeographicalNodeManager geographicalNodeManager, TechnicianManager technicianManager, TimeIntervalManager timeIntervalManager) {
        this.appointmentManager = appointmentManager;
        this.constantManager = constantManager;
        this.geographicalNodeManager = geographicalNodeManager;
        this.technicianManager = technicianManager;
        this.timeIntervalManager = timeIntervalManager;
        int nodesSize = geographicalNodeManager.getNodes().size();
        this.edges = new EdgeTravelingInformation[timeIntervalManager.getTimeIntervals().size()][nodesSize][nodesSize];
        this.edgesDepot = new EdgeTravelingInformation[nodesSize];
    }

    /**
     * Set one EdgeTravelingInformation of the depot
     * idSource or idDestination need to be constantManager.DEPOT_ID
     *
     * @param edge          the edge
     * @param idSource      the source or DEPOT_ID
     * @param idDestination the destination o rDEPOT_ID
     */
    synchronized public void setTravelingInformationToDepot(EdgeTravelingInformation edge, int idSource, int idDestination) {
        if (idSource == constantManager.getDepotId()) {
            edgesDepot[idDestination] = edge;
        } else if (idDestination == constantManager.getDepotId()) {
            edgesDepot[idSource] = edge;
        } else {
            System.err.println("Error");
        }
    }

    /**
     * Set one EdgeTravelingInformation
     *
     * @param edge           the edge @see {@link EdgeTravelingInformation}
     * @param idSource       the id which is store in the node source
     * @param idDestination  the id which is store in the node destination
     * @param idTimeInterval the id which is store in the time interval
     */
    public void setTravelingInformation(EdgeTravelingInformation edge, int idSource, int idDestination, int idTimeInterval) {
        edges[idTimeInterval][idSource][idDestination] = edge;
    }

    /**
     * Get @see {@link EdgeTravelingInformation}
     *
     * @param idSource      the id which is store in the node source
     * @param idDestination the id which is store in the node destination
     * @param time          the time of the days which will be search in timeinterval. Ignored if idSource is the depot id
     * @return edge at between source node and destination node at the time interval Null if timeInterval not found
     */
    synchronized public EdgeTravelingInformation getTravelingInformation(int idSource, int idDestination, int time) {
        if (idSource == constantManager.getDepotId()) {
            return edgesDepot[idDestination];
        } else if (idDestination == constantManager.getDepotId()) {
            return edgesDepot[idSource];
        }

        TimeInterval timeInterval = timeIntervalManager.getTimeIntervals(time);
        if (timeInterval != null) {
            return edges[timeInterval.getId()][idSource][idDestination];
        }
        return null;
    }

    /**
     * Get the EdgeTravelingInformation to the depot from the appointment
     *
     * @param appointment the source
     * @return the edge
     */
    public EdgeTravelingInformation getEdgeToDepot(Appointment appointment) {
        return edgesDepot[appointment.getNodeId()];
    }

    /**
     * Returns the FIFO compliant time of travel of the given edge at the given time
     *
     * @param idSource      the id which is store in the node source
     * @param idDestination the id which is store in the node destination
     * @param time          the time of the days which will be search in timeinterval
     * @return The modified time of travel (which is FIFO compliant) of the given edge at the given time
     */
    synchronized public int getFIFOTravellingTime(int idSource, int idDestination, int time) {

        List<TimeInterval> timeIntervals = timeIntervalManager.getTimeIntervals();

        TimeInterval currentTI = timeIntervalManager.getTimeIntervals(time);
        if (currentTI == null) {//TODO nicer way of doing this ?
            currentTI = timeIntervals.get(timeIntervals.size() - 1);
        }
        EdgeTravelingInformation eti = getTravelingInformation(idSource, idDestination, time);

        int startTime = time;
        int arrivalTime = time + eti.getTime();
        double remainingDistance = eti.getDistance();
        double speed = eti.getDistance() / (double) eti.getTime();

        while (arrivalTime > currentTI.getEndTime()) {

            //Speed is in meters per minute

            remainingDistance -= speed * (currentTI.getEndTime() - startTime);

            startTime = currentTI.getEndTime();

            //If we are at the last time interval then stop here
            if (currentTI.getId() == timeIntervals.get(timeIntervals.size() - 1).getId()) {
                break;
            }

            currentTI = timeIntervalManager.getTimeIntervals(startTime);
            eti = getTravelingInformation(idSource, idDestination, time);
            speed = eti.getDistance() / (double) eti.getTime();

            arrivalTime = startTime + (int) (remainingDistance / speed);
        }

        return (arrivalTime - time);
    }


    /**
     * Get all destination @see {@link EdgeTravelingInformation} for specific time interval and source node
     *
     * @param idTimeInterval the id which is store in the time interval
     * @param idSource       the id which is store in the node source
     * @return tab of @see {@link EdgeTravelingInformation} for one specific time interval and source node
     */
    synchronized public EdgeTravelingInformation[] getTravelingInformationMatrix(int idTimeInterval, int idSource) {
        return edges[idTimeInterval][idSource];
    }

    /**
     * Get all source/destination @see {@link EdgeTravelingInformation} for one specific time interval
     *
     * @param time
     * @return tab of tab of @see {@link EdgeTravelingInformation} for one specific time interval return null if time not found
     */
    synchronized public EdgeTravelingInformation[][] getTravelingInformationMatrix(int time) {
        TimeInterval timeInterval = timeIntervalManager.getTimeIntervals(time);
        if (timeInterval != null) {
            return edges[timeInterval.getId()];
        }
        return null;
    }


    /**
     * get a tab of all EdgeTravelingInformation for all timeInterval for 2 nodes
     *
     * @param idSource      source
     * @param idDestination destination
     * @return tab
     */
    synchronized public EdgeTravelingInformation[] getAllTimeIntervalTravelingInformationMatrix(int idSource, int idDestination) {
        EdgeTravelingInformation[] out = new EdgeTravelingInformation[edges.length];

        for (int i = 0; i < out.length; i++) {
            out[i] = edges[i][idSource][idDestination];
        }
        return out;
    }

    /**
     * Get all the nodes
     *
     * @return list of @see {@link GeographicalNode}
     */
    public List<GeographicalNode> getNodes() {
        return geographicalNodeManager.getNodes();
    }

    /**
     * Get all time interval
     *
     * @return list of @see {@link TimeInterval}
     */
    public List<TimeInterval> getTimeIntervals() {
        return timeIntervalManager.getTimeIntervals();
    }

    public AppointmentManager getAppointmentManager() {
        return appointmentManager;
    }

    public ConstantManager getConstantManager() {
        return constantManager;
    }

    public GeographicalNodeManager getGeographicalNodeManager() {
        return geographicalNodeManager;
    }

    public TechnicianManager getTechnicianManager() {
        return technicianManager;
    }

    public TimeIntervalManager getTimeIntervalManager() {
        return timeIntervalManager;
    }
}
