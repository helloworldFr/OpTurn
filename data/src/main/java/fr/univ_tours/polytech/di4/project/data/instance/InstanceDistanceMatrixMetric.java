package fr.univ_tours.polytech.di4.project.data.instance;

import java.util.Map;

/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This class represent a metric of an instance contain the number of each occurrence (and min/max) in one timeInterval for all origin/destination geographicalNode
 */
public class InstanceDistanceMatrixMetric {
    private int min;
    private int max;
    //contain the number in key and the number of occurrence in value
    private Map<Integer, Integer> occurrences;

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public Map<Integer, Integer> getOccurrences() {
        return occurrences;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public void setOccurrences(Map<Integer, Integer> occurrences) {
        this.occurrences = occurrences;
    }
}
