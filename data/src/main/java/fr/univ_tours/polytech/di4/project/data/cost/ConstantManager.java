package fr.univ_tours.polytech.di4.project.data.cost;

import java.io.Serializable;

/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This class contains various constants needed in the program
 */
public class ConstantManager implements Serializable {

    /**
     * kilometers cost €/km
     */
    private double kilometre;

    /**
     * technician cost €/day
     */
    private double technician;

    /**
     * technician cost €/h
     */
    private double hourlyFee;

    /**
     * Time of the start of the day in minutes
     */
    private int minuteStart;

    /**
     * Time of the end of the day in minutes
     */
    private int minuteEnd;

    /**
     * Time step used by the program
     */
    private int stepMinute;

    /**
     * ID of the depot in the matrix
     */
    private int depotId;

    /**
     * Default constructor
     */
    public ConstantManager() {
    }

    public void setDepotId(int depotId) {
        this.depotId = depotId;
    }

    public int getDepotId() {
        return depotId;
    }

    public int getStepMinute() {
        return stepMinute;
    }

    public void setStepMinute(int stepMinute) {
        this.stepMinute = stepMinute;
    }

    public int getMinuteEnd() {
        return minuteEnd;
    }

    public void setMinuteEnd(int minuteEnd) {
        this.minuteEnd = minuteEnd;
    }

    public int getMinuteStart() {
        return minuteStart;
    }

    public void setMinuteStart(int minuteStart) {
        this.minuteStart = minuteStart;
    }

    public double getKilometre() {
        return kilometre;
    }

    public void setKilometre(double kilometre) {
        this.kilometre = kilometre;
    }

    public double getTechnician() {
        return technician;
    }

    public void setTechnician(double technician) {
        this.technician = technician;
    }

    public double getHourlyFee() {
        return hourlyFee;
    }

    public void setHourlyFee(double hourlyFee) {
        this.hourlyFee = hourlyFee;
    }
}
