package fr.univ_tours.polytech.di4.project.data.appointment;

import fr.univ_tours.polytech.di4.project.data.geographical.GeographicalNode;
import fr.univ_tours.polytech.di4.project.data.technician.TechnicianSkills;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This class store all @see {@link Appointment} in a list
 */
public class AppointmentManager {

    private List<Appointment> appointments;

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }

    /**
     * Default constructor which initialize the list of @see {@link Appointment}
     */
    public AppointmentManager() {
        appointments = new ArrayList<Appointment>();
    }

    /**
     * Add an appointment to the list
     *
     * @param id        of the appointment which is the id in the source file
     * @param skill     of the technician (use @see {@link TechnicianSkills} enum)
     * @param node      the location of the appointment @see {@link GeographicalNode}
     * @param startTime the start time of the appointment in minutes
     * @param endTime   the end time of the appointment in minutes
     * @param duration  the duration of the appointment in minutes
     * @return the appointment
     */
    public Appointment add(int id, TechnicianSkills skill, GeographicalNode node, int startTime, int endTime, int duration) {
        Appointment appointment = new Appointment(id, skill, node.getId(), startTime, endTime, duration);
        appointments.add(appointment);
        return appointment;
    }

    /**
     * Find an appointment by is id
     *
     * @param id of the appointment to find
     * @return appointment if found -1 if not
     */
    public Appointment findAppointment(int id) {
        for (Appointment appointment : appointments) {
            if (appointment.getId() == id) {
                return appointment;
            }
        }
        return null;
    }

    /**
     * Find an appointment by is id
     *
     * @param nodeID of the node of the appointment to find
     * @return appointment if found -1 if not
     */
    public Appointment findAppointmentByNode(int nodeID) {
        for (Appointment appointment : appointments) {
            if (appointment.getNodeId() == nodeID) {
                return appointment;
            }
        }
        return null;
    }

    public List<Appointment> getAppointments() {
        return appointments;
    }
}
