package fr.univ_tours.polytech.di4.project.data.time_interval;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This class contain all time interval and handle the allocation of the id
 */
public class TimeIntervalManager {
    private List<TimeInterval> timeIntervals;
    private int currentId = 0;
    //Default parameters which will be overwritten in the constructor
    private int startTime = 480;
    private int step = 60;


    /**
     * Default constructor which initialize the list of @see {@link TimeInterval}
     *
     * @param startTime start of the first interval
     * @param step      the step of each interval
     */
    public TimeIntervalManager(int startTime, int step) {
        timeIntervals = new ArrayList<TimeInterval>();
        this.startTime = startTime;
        this.step = step;
    }

    /**
     * Create a new interval and add it to the current list
     *
     * @param startTime of the interval
     * @param endTime   of the interval
     * @return the TimeInterval create with the longitude and latitude
     */
    public TimeInterval addTimeInterval(int startTime, int endTime) {
        TimeInterval timeInterval = new TimeInterval(currentId, startTime, endTime);
        timeIntervals.add(timeInterval);
        currentId++;
        return timeInterval;
    }

    public List<TimeInterval> getTimeIntervals() {
        return timeIntervals;
    }

    /**
     * Get the right TimeInterval for a tgiven time
     *
     * @param time to find
     * @return timeInterval or null if not fund
     */
    public TimeInterval getTimeIntervals(int time) {
        int id = (time - startTime) / step;
        if (id > timeIntervals.size() || id < 0) {
            return null;
        }

        //TODO take a look of this patch weird
        if (id == timeIntervals.size()) {
            id--;
        }
        return timeIntervals.get(id);
    }

}
