package fr.univ_tours.polytech.di4.project.data.geographical;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This class create correct list of @see {@link GeographicalNode}
 */
public class GeographicalNodeManager {
    private int currentId = 0;
    private List<GeographicalNode> nodes;

    /**
     * Default constructor which initialize the list of @see {@link GeographicalNode}
     */
    public GeographicalNodeManager() {
        nodes = new ArrayList<GeographicalNode>();
    }

    /**
     * Create a new node if not already in tab and add to the current list
     *
     * @param longitude of the geographic node
     * @param latitude  of the geographic node
     * @return the GeographicalNode create with the longitude and latitude or the node already in tab
     */
    public GeographicalNode addNodes(double latitude, double longitude) {
        //find if node already exist in tab
        for (GeographicalNode node : nodes) {
            if (node.getLatitude() == latitude && node.getLongitude() == longitude) {
                return node;
            }
        }
        GeographicalNode newNode = new GeographicalNode(currentId, latitude, longitude);
        nodes.add(newNode);
        currentId++;
        return newNode;
    }

    public List<GeographicalNode> getNodes() {
        return nodes;
    }
}
