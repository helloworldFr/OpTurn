package fr.univ_tours.polytech.di4.project.data.technician;

import java.io.Serializable;

/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This class store one technician
 */
public class Technician implements Serializable {
    /**
     * Start time of the technician on morning (in minutes)
     */
    private int startTimeMorning;

    /**
     * End time of the technician on morning (in minutes)
     */
    private int endTimeMorning;

    /**
     * Start time of the technician on afternoon (in minutes)
     */
    private int startTimeAfternoon;

    /**
     * End time of the technician on afternoon (in minutes)
     */
    private int endTimeAfternoon;

    /**
     * The skill of the technician @see {@link TechnicianSkills}
     */
    private TechnicianSkills skill;

    /**
     * Constructor of Technician
     *
     * @param startTimeMorning   Start time of the technician on morning (in minutes)
     * @param endTimeMorning     End time of the technician on morning (in minutes)
     * @param startTimeAfternoon Start time of the technician on afternoon (in minutes)
     * @param endTimeAfternoon   End time of the technician on afternoon (in minutes)
     * @param skill              The skill of the technician @see {@link TechnicianSkills}
     */
    Technician(int startTimeMorning, int endTimeMorning, int startTimeAfternoon, int endTimeAfternoon, TechnicianSkills skill) {
        this.startTimeMorning = startTimeMorning;
        this.endTimeMorning = endTimeMorning;
        this.startTimeAfternoon = startTimeAfternoon;
        this.endTimeAfternoon = endTimeAfternoon;
        this.skill = skill;
    }

    public int getStartTimeMorning() {
        return startTimeMorning;
    }

    public int getEndTimeMorning() {
        return endTimeMorning;
    }

    public int getStartTimeAfternoon() {
        return startTimeAfternoon;
    }

    public int getEndTimeAfternoon() {
        return endTimeAfternoon;
    }

    public TechnicianSkills getSkill() {
        return skill;
    }
}
