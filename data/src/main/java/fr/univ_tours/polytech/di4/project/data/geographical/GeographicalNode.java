package fr.univ_tours.polytech.di4.project.data.geographical;

import java.io.Serializable;

/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This class represent a geographical point on a map with coordinate and an unique ID
 */
public class GeographicalNode implements Serializable {
    private int id;
    private double longitude;
    private double latitude;

    /**
     * Constructor of geographical node
     *
     * @param id        different from source file, this id is use to have a continue id's in the instance
     * @param latitude  the geographical latitude of the node
     * @param longitude the geographical longitude of the node
     */
    GeographicalNode(int id, double latitude, double longitude) {
        this.id = id;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public int getId() {
        return id;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

}