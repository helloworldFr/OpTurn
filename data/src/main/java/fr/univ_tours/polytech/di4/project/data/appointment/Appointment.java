package fr.univ_tours.polytech.di4.project.data.appointment;

import fr.univ_tours.polytech.di4.project.data.geographical.GeographicalNode;
import fr.univ_tours.polytech.di4.project.data.technician.TechnicianSkills;

import java.io.Serializable;

/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This class store one appointment
 */
public class Appointment implements Serializable {
    private int id;
    private int nodeId;
    private int startTime;
    private int endTime;
    private TechnicianSkills skill;
    private int durationTime;

    /**
     * Constructor of appointment
     *
     * @param id           of the appointment which is the id in the source file
     * @param skill        of the technician (use @see {@link TechnicianSkills} enum)
     * @param nodeId       the id of the location of the appointment @see {@link GeographicalNode}
     * @param startTime    the start time of the appointment in minutes
     * @param endTime      the end time of the appointment in minutes
     * @param durationTime the duration of the appointment in minutes
     */
    Appointment(int id, TechnicianSkills skill, int nodeId, int startTime, int endTime, int durationTime) {
        this.id = id;
        this.skill = skill;
        this.nodeId = nodeId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.durationTime = durationTime;
    }

    public int getNodeId() {
        return nodeId;
    }

    public int getEndTime() {
        return endTime;
    }

    public int getStartTime() {
        return startTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TechnicianSkills getSkill() {
        return skill;
    }

    public int getDurationTime() {
        return durationTime;
    }

    @Override
    public boolean equals(Object o) {
        if (!o.getClass().equals(getClass()))
            return false;

        return ((Appointment) o).getId() == getId();
    }

    @Override
    public int hashCode() {
        return id * nodeId * startTime * endTime * durationTime;
    }
}
