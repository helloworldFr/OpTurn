package fr.univ_tours.polytech.di4.project.data.technician;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This class store all @see {@link Technician} in a list
 */
public class TechnicianManager {
    private List<Technician> technicians;

    /**
     * Default constructor which initialize the list of @see {@link TechnicianManager}
     */
    public TechnicianManager() {
        technicians = new ArrayList<Technician>();
    }

    /**
     * Add a technician to the list
     *
     * @param startTimeMorning   Start time of the technician on morning (in minutes)
     * @param endTimeMorning     End time of the technician on morning (in minutes)
     * @param startTimeAfternoon Start time of the technician on afternoon (in minutes)
     * @param endTimeAfternoon   End time of the technician on afternoon (in minutes)
     * @param skill              The skill of the technician @see {@link TechnicianSkills}
     * @return technician
     */
    public Technician add(int startTimeMorning, int endTimeMorning, int startTimeAfternoon, int endTimeAfternoon, TechnicianSkills skill) {
        Technician technician = new Technician(startTimeMorning, endTimeMorning, startTimeAfternoon, endTimeAfternoon, skill);
        technicians.add(technician);
        return technician;
    }

    public List<Technician> getTechnicians() {
        return technicians;
    }
}
