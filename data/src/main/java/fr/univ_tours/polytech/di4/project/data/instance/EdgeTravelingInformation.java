package fr.univ_tours.polytech.di4.project.data.instance;

import java.io.Serializable;

/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This class represent the distance between two geographical node.
 *          Also contain the time between this two nodes.
 */
public class EdgeTravelingInformation implements Comparable<EdgeTravelingInformation>, Serializable {
    /**
     * Distance in kilometers
     */
    private double distance;
    /**
     * Time in minute
     */
    private int time;
    /**
     * Contain all geographical points between this 2 nodes
     */
    private String polyline;

    /**
     * Constructor of one EdgeTravelingInformation
     *
     * @param distance between two nodes in kilometers
     * @param time     between two nodes
     * @param polyline between two nodes
     */
    public EdgeTravelingInformation(double distance, int time, String polyline) {
        this.distance = distance;
        this.time = time;
        this.polyline = polyline;
    }


    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getPolyline() {
        return polyline;
    }

    public int compareTo(EdgeTravelingInformation o) {
        return this.getTime() - o.getTime();
    }
}
