package fr.univ_tours.polytech.di4.project.io.solution;

import com.google.gson.Gson;
import fr.univ_tours.polytech.di4.project.algorithm.solution.Solution;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Theo on 07/05/2016.
 * This class is used to write a solution to the hard drive
 */
public class SolutionWriter {

    /**
     * Writes a given solution to the hard drive as a json file
     * The file will have for name the given filename
     *
     * @param solution The solution to write as a json file
     * @param filename The filename of the json file to create
     */

    public void writeSolution(Solution solution, String filename) {
        try (FileWriter file = new FileWriter(filename)) {
            file.write(new Gson().toJson(solution));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
