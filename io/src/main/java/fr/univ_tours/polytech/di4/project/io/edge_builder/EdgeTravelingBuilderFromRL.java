package fr.univ_tours.polytech.di4.project.io.edge_builder;

import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;
import fr.univ_tours.polytech.di4.project.data.appointment.AppointmentManager;
import fr.univ_tours.polytech.di4.project.data.geographical.GeographicalNode;
import fr.univ_tours.polytech.di4.project.data.instance.EdgeTravelingInformation;
import org.joda.time.DateTime;

/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This class is use to build @see {@link EdgeTravelingInformation}
 */
public class EdgeTravelingBuilderFromRL extends EdgeTravelingBuilder {
    private double[][] distances;
    private int[][] times;

    private AppointmentManager appointmentManager;

    /**
     * Constructor
     *
     * @param distances          matrix
     * @param times              matrix
     * @param appointmentManager
     */
    public EdgeTravelingBuilderFromRL(double[][] distances, int[][] times, AppointmentManager appointmentManager) {
        super(null);
        this.distances = distances;
        this.times = times;
        this.appointmentManager = appointmentManager;
    }

    /**
     * Create an @see {@link EdgeTravelingInformation} at the time
     *
     * @param source      the position source
     * @param destination the position to go
     * @param time        the time of depart
     * @return the edge information
     */
    @Override
    public EdgeTravelingInformation createEdge(GeographicalNode source, GeographicalNode destination, DateTime time) {
        Appointment appointmentSource = appointmentManager.findAppointmentByNode(source.getId());
        Appointment appointmentDestination = appointmentManager.findAppointmentByNode(destination.getId());

        int lineIndexSource = findIndex(appointmentSource);
        int columnIndexDestination = findIndex(appointmentDestination) + 2;

        return new EdgeTravelingInformation(distances[lineIndexSource][columnIndexDestination], times[lineIndexSource][columnIndexDestination], null);
    }

    /**
     * Find index in matrix of the appointment
     *
     * @param appointment to find
     * @return index or -1 if not found
     */
    private int findIndex(Appointment appointment) {
        for (int i = 0; i < distances.length; i++) {
            if (appointment.getId() == distances[i][0]) {
                return i;
            }
        }
        return -1;
    }
}
