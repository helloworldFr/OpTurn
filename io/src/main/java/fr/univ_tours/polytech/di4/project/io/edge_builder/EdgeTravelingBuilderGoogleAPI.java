package fr.univ_tours.polytech.di4.project.io.edge_builder;

import com.google.gson.Gson;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.*;
import fr.univ_tours.polytech.di4.project.data.BackupEdge;
import fr.univ_tours.polytech.di4.project.data.geographical.GeographicalNode;
import fr.univ_tours.polytech.di4.project.data.instance.EdgeTravelingInformation;
import org.joda.time.DateTime;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This class is use to build @see {@link EdgeTravelingInformation}
 */
public class EdgeTravelingBuilderGoogleAPI extends EdgeTravelingBuilder {
    private GeoApiContext context;
    private static AtomicInteger numberRequest = new AtomicInteger(0);


    /**
     * Constructor which initialize API key
     *
     * @param APIKey         key
     * @param filenameBackup the filename which contain backup node if null temp backup file backup.txt
     */
    public EdgeTravelingBuilderGoogleAPI(String APIKey, String filenameBackup) {
        super(filenameBackup);
        context = new GeoApiContext().setApiKey(APIKey);
    }


    /**
     * Create an @see {@link EdgeTravelingInformation} at the time
     * Using GoogleAPI
     *
     * @param nodeSource      the position source
     * @param nodeDestination the position to go
     * @param time            the time of depart
     * @return the edge information
     */
    @Override
    public EdgeTravelingInformation createEdge(GeographicalNode nodeSource, GeographicalNode nodeDestination, DateTime time) {
        String source = nodeSource.getLatitude() + "," + nodeSource.getLongitude();
        String destination = nodeDestination.getLatitude() + "," + nodeDestination.getLongitude();
        if (source.equals(destination)) {
            return new EdgeTravelingInformation(0, 0, null);
        } else if (loadFromBackup && !backupEdges.isEmpty()) {
            BackupEdge edge = new BackupEdge();
            edge.setSource(source);
            edge.setDestination(destination);
            edge.setTime(formatter.print(time));
            BackupEdge restoreEdge = backupEdges.get(edge);
            if (restoreEdge != null) {
                backupEdges.remove(restoreEdge);
                //System.out.println("Restore from backup");
                return new EdgeTravelingInformation(restoreEdge.getDistance(), restoreEdge.getDuration(), null);
            }
            //System.out.println("remain : "+ backupEdges.size());
        }

        return getEdgeFromAPI(source, destination, time);
    }

    /**
     * Get edge information by using API to get duration and distance
     *
     * @param source      point
     * @param destination point
     * @param time        when want information
     * @return edge
     */
    private EdgeTravelingInformation getEdgeFromAPI(String source, String destination, DateTime time) {
        DirectionsApiRequest requestDirection = DirectionsApi.getDirections(context, source, destination);
        requestDirection.trafficModel(TrafficModel.PESSIMISTIC);
        requestDirection.mode(TravelMode.DRIVING);
        requestDirection.units(Unit.METRIC);

        requestDirection.departureTime(time);
        DirectionsResult directionsResult;
        double distance = 0;
        int duration = 0;
        String polyline = "";

        try {
            directionsResult = requestDirection.await();
            DirectionsLeg leg = directionsResult.routes[0].legs[0];
            distance = leg.distance.inMeters / 1000.0; // in Km
            duration = (int) (leg.durationInTraffic.inSeconds / 60.0);

            /*for (DirectionsStep step : leg.steps) {
                //polyline += step.polyline.getEncodedPath();
            }*/
            numberRequest.incrementAndGet();

            //Backup
            BackupEdge backup = new BackupEdge();
            backup.setSource(source);
            backup.setDestination(destination);
            backup.setTime(formatter.print(time));
            backup.setDistance(distance);
            backup.setDuration(duration);

            bufferedWriter.append(new Gson().toJson(backup) + "\n");
            bufferedWriter.flush();

        } catch (Exception e) {
            System.out.println("ERROR !! " + e.getMessage());
            e.printStackTrace();
        }

        return new EdgeTravelingInformation(distance, duration, polyline);
    }

    /**
     * @return the number of request on the API
     */
    public static int getNumberRequest() {
        return numberRequest.get();
    }


}
