package fr.univ_tours.polytech.di4.project.io.edge_builder;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import fr.univ_tours.polytech.di4.project.data.BackupEdge;
import fr.univ_tours.polytech.di4.project.data.geographical.GeographicalNode;
import fr.univ_tours.polytech.di4.project.data.instance.EdgeTravelingInformation;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This abstract class is use to build @see {@link EdgeTravelingInformation}
 */
public abstract class EdgeTravelingBuilder {
    protected BufferedWriter bufferedWriter;
    protected boolean loadFromBackup = false;
    protected static DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm:ss");

    /**
     * This map contain all request already done in a previous time
     * The key is the hashcode of the backupEdge
     */
    protected Map<BackupEdge, BackupEdge> backupEdges;

    /**
     * Constructor which initialize API key
     *
     * @param filenameBackup the filename which contain backup node if null temp backup file backup.txt
     */
    public EdgeTravelingBuilder(String filenameBackup) {

        if (filenameBackup != null) {
            //Read backup and put it in a list
            backupEdges = new ConcurrentHashMap<BackupEdge, BackupEdge>();
            try {
                File file = new File(filenameBackup);
                if (!file.exists()) {
                    file.createNewFile();
                }
                try (Scanner scanner = new Scanner(file)) {
                    while (scanner.hasNextLine()) {
                        String line = scanner.nextLine();
                        try {
                            BackupEdge edge = new Gson().fromJson(line, BackupEdge.class);
                            backupEdges.put(edge, edge);
                        } catch (JsonIOException | JsonSyntaxException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                bufferedWriter = new BufferedWriter(new FileWriter(filenameBackup, true));//append
            } catch (IOException e) {
                e.printStackTrace();
            }
            loadFromBackup = true;
        } else {
            try {
                bufferedWriter = new BufferedWriter(new FileWriter("backup.txt"));//remove
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void closeBackup() {
        try {
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create an @see {@link EdgeTravelingInformation} at the time
     *
     * @param source      the position source
     * @param destination the position to go
     * @param time        the time of depart
     * @return the edge information
     */
    public EdgeTravelingInformation createEdge(GeographicalNode source, GeographicalNode destination, DateTime time) {
        throw new NotImplementedException();
    }
}
