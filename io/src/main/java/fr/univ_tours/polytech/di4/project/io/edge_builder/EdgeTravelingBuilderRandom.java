package fr.univ_tours.polytech.di4.project.io.edge_builder;

import fr.univ_tours.polytech.di4.project.data.geographical.GeographicalNode;
import fr.univ_tours.polytech.di4.project.data.instance.EdgeTravelingInformation;
import org.joda.time.DateTime;

/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This class is use to build @see {@link EdgeTravelingInformation}
 */
public class EdgeTravelingBuilderRandom extends EdgeTravelingBuilder {

    /**
     * Default constructor
     */
    public EdgeTravelingBuilderRandom() {
        super(null);
    }

    /**
     * Create an @see {@link EdgeTravelingInformation} at the time
     *
     * @param source      the position source
     * @param destination the position to go
     * @param time        the time of depart
     * @return random distance and travel time compute from random speed
     */
    @Override
    public EdgeTravelingInformation createEdge(GeographicalNode source, GeographicalNode destination, DateTime time) {
        double dist = Math.random() * 18000 + 2000;
        //The speed can range from 6 to 36 m/s (that is from 22 to 130 km/h)
        double speed = Math.random() * 30 + 6;
        int travelTime = (int) (dist / speed) / 60;

        return new EdgeTravelingInformation(dist / 1000.0, travelTime, null);
    }
}
