package fr.univ_tours.polytech.di4.project.io;

import com.google.gson.Gson;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;
import fr.univ_tours.polytech.di4.project.data.appointment.AppointmentManager;
import fr.univ_tours.polytech.di4.project.data.cost.ConstantManager;
import fr.univ_tours.polytech.di4.project.data.geographical.GeographicalNode;
import fr.univ_tours.polytech.di4.project.data.geographical.GeographicalNodeManager;
import fr.univ_tours.polytech.di4.project.data.instance.EdgeTravelingInformation;
import fr.univ_tours.polytech.di4.project.data.instance.InstanceDistanceMatrix;
import fr.univ_tours.polytech.di4.project.data.instance.InstanceDistanceMatrixMetric;
import fr.univ_tours.polytech.di4.project.data.technician.Technician;
import fr.univ_tours.polytech.di4.project.data.technician.TechnicianManager;
import fr.univ_tours.polytech.di4.project.data.technician.TechnicianSkills;
import fr.univ_tours.polytech.di4.project.data.time_interval.TimeIntervalManager;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Mathieu on 18/03/2016.
 */
public class IOInstanceDistanceMatrix {

    /**
     * Instance
     */
    private InstanceDistanceMatrix instance;

    /***
     * Stores all distances and times if you want use EdgeTravelingBuilderFromRL
     */
    private String[][] distances;
    private String[][] times;


    /**
     * Constant for time
     */
    private int stepMinute;

    /**
     * Constructor
     *
     * @param properties
     */
    public IOInstanceDistanceMatrix(Properties properties) {
        stepMinute = Integer.parseInt(properties.getProperty("STEP"));
    }

    /**
     * create instanceDistanceMatrix by reading a file
     *
     * @param filename
     * @return InstanceDistanceMatrix
     */
    public static InstanceDistanceMatrix readJson(String filename) {
        Gson gson = new Gson();
        InstanceDistanceMatrix instanceDistanceMatrix = null;
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            instanceDistanceMatrix = gson.fromJson(br, InstanceDistanceMatrix.class); // converted the json to an object
        } catch (IOException e) {
            e.printStackTrace();
        }
        return instanceDistanceMatrix;
    }

    /**
     * write an object InstanceDistanceMatrix in a file
     *
     * @param metrics  InstanceDistanceMatrixMetric[][] to write
     * @param filename the file name
     */
    public static void writeInstanceDistanceMatrixMetrics(InstanceDistanceMatrixMetric[][] metrics, String filename) {
        Gson gson = new Gson();
        String json = gson.toJson(metrics); // converted the object to json
        FileWriter csvWriterOccurrenceFreq = null;
        FileWriter csvWriterDifferenceFreq = null;

        try {
            Map<Integer, Integer> occurrenceFrequency = new HashMap<Integer, Integer>();
            Map<Integer, Integer> differenceFrequency = new HashMap<Integer, Integer>();
            csvWriterOccurrenceFreq = new FileWriter(new File(filename + "_metrics_occurrence_freq.csv"));
            csvWriterDifferenceFreq = new FileWriter(new File(filename + "_metrics_difference_freq.csv"));
            write(filename + "_metrics.json", json);

            for (InstanceDistanceMatrixMetric[] metricLine : metrics) {
                for (InstanceDistanceMatrixMetric metric : metricLine) {
                    int keyOccurrence = metric.getOccurrences().size();
                    int countOccurrence = occurrenceFrequency.containsKey(keyOccurrence) ? occurrenceFrequency.get(keyOccurrence) : 0;
                    occurrenceFrequency.put(keyOccurrence, countOccurrence + 1);

                    int keyDistance = metric.getMax() - metric.getMin();
                    int countDistance = differenceFrequency.containsKey(keyDistance) ? differenceFrequency.get(keyDistance) : 0;
                    differenceFrequency.put(keyDistance, countDistance + 1);
                }
            }

            for (Map.Entry<Integer, Integer> entry : occurrenceFrequency.entrySet()) {
                csvWriterOccurrenceFreq.write(entry.getKey() + ";" + entry.getValue() + "\n");
            }

            for (Map.Entry<Integer, Integer> entry : differenceFrequency.entrySet()) {
                csvWriterDifferenceFreq.write(entry.getKey() + ";" + entry.getValue() + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (csvWriterOccurrenceFreq != null) {
                try {
                    csvWriterOccurrenceFreq.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (csvWriterDifferenceFreq != null) {
                try {
                    csvWriterDifferenceFreq.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * write the InstanceDistanceMatrix into a file
     *
     * @param filename the file name
     */
    public void writeJson(String filename) {
        Gson gson = new Gson();
        if (instance != null) {
            String json = gson.toJson(instance); // converted the object to json
            write(filename, json);
        } else {
            System.err.println("You need first to initialize the instance by reading it from file");
        }
    }

    /**
     * Write string into a file
     *
     * @param filename the file name
     * @param content  the String to write
     */
    private static void write(String filename, String content) {
        try (FileWriter file = new FileWriter(filename)) {
            file.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Parser to get some data
     *
     * @param filename file name
     * @return InstanceDistanceMatrix object
     */
    public InstanceDistanceMatrix parserReadFromSource(String filename) {
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            int minuteStart = Integer.MAX_VALUE;
            int minuteEnd = Integer.MIN_VALUE;
            String line;

            AppointmentManager appointmentManager = new AppointmentManager();
            ConstantManager constantManager = new ConstantManager();
            TechnicianManager technicianManager = new TechnicianManager();
            GeographicalNodeManager geographicalNodeManager = new GeographicalNodeManager();
            TimeIntervalManager timeIntervalManager = null;

            constantManager.setStepMinute(stepMinute);

            br.readLine(); // # flat cost about technicien exit
            line = br.readLine(); // cost
            constantManager.setTechnician(Double.parseDouble(line));

            br.readLine(); // #km cost
            line = br.readLine(); // le cout
            constantManager.setKilometre(Double.parseDouble(line));

            br.readLine(); // #hour cost
            line = br.readLine(); // cost
            constantManager.setHourlyFee(Double.parseDouble(line));

            br.readLine(); //# number of technicien
            line = br.readLine();
            int nbTechnicien = Integer.parseInt(line); // number of technicien

            br.readLine(); //#techniciens
            for (int i = 0; i < nbTechnicien; i++) {
                line = br.readLine(); // data of techniciens
                String[] subString = line.split(" ");
                Technician technician = technicianManager.add(Integer.parseInt(subString[0]), Integer.parseInt(subString[1]), Integer.parseInt(subString[2]), Integer.parseInt(subString[3]), TechnicianSkills.values()[Integer.parseInt(subString[4]) - 1]);
                //find min start and max end
                if (technician.getStartTimeMorning() < minuteStart) {
                    minuteStart = technician.getStartTimeMorning();
                }

                if (technician.getEndTimeAfternoon() > minuteEnd) {
                    minuteEnd = technician.getEndTimeAfternoon();
                }
            }

            br.readLine(); // line break

            br.readLine(); //# number of intervention
            int nbIntervention = Integer.parseInt(br.readLine()); // Number of intervention

            br.readLine(); //#intervention

            timeIntervalManager = new TimeIntervalManager(minuteStart, stepMinute);

            for (int i = minuteStart; i < minuteEnd; i += stepMinute) {
                //if i issn't in the lunch time [start lunch; end lunch]
                //if (!(i >= minuteStartLunch && i < minuteEndLunch))
                timeIntervalManager.addTimeInterval(i, i + stepMinute);
            }

            for (int i = 0; i < nbIntervention; i++) {
                line = br.readLine(); // intervention's data

                String[] subString = line.split(" ");

                // if two spacing between latitude & longitude.
                if (subString[6].compareTo("") == 0 && !(subString[7].compareTo("") == 0))
                    subString[6] = subString[7];

                GeographicalNode geographicalNode = geographicalNodeManager.addNodes(Double.parseDouble(subString[5]), Double.parseDouble(subString[6]));//lat first , longitude second
                appointmentManager.add(Integer.parseInt(subString[0]), TechnicianSkills.values()[Integer.parseInt(subString[4]) - 1], geographicalNode, Integer.parseInt(subString[1]), Integer.parseInt(subString[2]), Integer.parseInt(subString[3]));

            }

            //Add depot at this END
            constantManager.setDepotId(geographicalNodeManager.getNodes().size() + 1);
            instance = new InstanceDistanceMatrix(appointmentManager, constantManager, geographicalNodeManager, technicianManager, timeIntervalManager);

            br.readLine(); // line break
            br.readLine(); //#distance
            String[] ids = br.readLine().split("\t"); // list of interventions
            String[] distanceToDepot = br.readLine().split("\t"); // depot's distance
            distances = new String[ids.length - 2][ids.length - 2];
            int indexLine = 0;

            line = br.readLine();
            while (!line.equals("#durations") || line.equals("\n")) {
                String[] distance = line.split("\t");
                distances[indexLine] = distance;
                indexLine++;
                line = br.readLine();
                if (line == null) {
                    throw new IOException("missing #temps");
                }
            }

            ids = br.readLine().split("\t");//list of interventions
            String[] timeToDepot = br.readLine().split("\t"); // depot's time
            times = new String[ids.length - 2][ids.length - 2];

            indexLine = 0;
            line = br.readLine();
            while (line != null) {
                String[] time = line.split("\t");
                times[indexLine] = time;
                indexLine++;
                line = br.readLine();
            }

            //set distance/time to depot
            for (int i = 2; i < timeToDepot.length; i++) {
                Appointment appointment = appointmentManager.findAppointment(Integer.parseInt(ids[i]));
                if (appointment != null) {
                    int time = (int) Double.parseDouble(timeToDepot[i]);

                    double distance = Double.parseDouble(distanceToDepot[i]);
                    instance.setTravelingInformationToDepot(new EdgeTravelingInformation(distance, time, null), appointment.getNodeId(), constantManager.getDepotId());

                } else {
                    System.out.println("No appointment for this id");
                }
            }

            return instance;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String[][] getDistances() {
        return distances;
    }

    public String[][] getTimes() {
        return times;
    }

}
