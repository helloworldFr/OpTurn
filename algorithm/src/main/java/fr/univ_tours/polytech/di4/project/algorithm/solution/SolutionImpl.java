package fr.univ_tours.polytech.di4.project.algorithm.solution;

import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;
import fr.univ_tours.polytech.di4.project.data.technician.Technician;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Théo on 01/04/2016.
 */
public class SolutionImpl implements Solution {

    /**
     * The routes
     */
    private List<AdvancedRoute> routes;
    /**
     * The objective function
     */
    private double of = Double.NaN;

    public SolutionImpl() {
        this.routes = new ArrayList<AdvancedRoute>();
    }

    public double getOF() {
        return of;
    }

    public void setOF(double of) {
        this.of = of;
    }

    public void updateOF() {
        of = 0;
        for (Route r : routes) {
            of += r.getCost();
        }
    }

    public void updateAllOF() {
        of = 0;
        for (AdvancedRoute r : routes) {
            r.updateCost();
            of += r.getCost();
        }
    }

    public List<AdvancedRoute> getRoutes() {
        return cloneRoutes();
    }

    public AdvancedRoute getRoute(int route) {
        return routes.get(route);
    }

    public void removeRoute(int route) {
        routes.remove(route);
    }

    public void setRoutes(List<AdvancedRoute> routes) {
        this.routes = new ArrayList<AdvancedRoute>();
        for (AdvancedRoute r : routes)
            this.routes.add(r.clone());
    }

    public void addRoute(AdvancedRoute route) {
        this.routes.add(route.clone());
    }

    public void removeRoute(AdvancedRoute route) {
        this.routes.remove(route);
    }

    public void insertNode(Appointment appointment, int route, int position) {
        this.routes.get(route).insert(appointment, position);
    }

    public Appointment removeNode(int route, int position) {
        return routes.get(route).remove(position);
    }

    public double getRouteCost(int route) {
        return routes.get(route).getCost();
    }

    public void setRouteCost(int route, double cost) {
        routes.get(route).setCost(cost);
    }

    public int routeCount() {
        return routes.size();
    }

    public int routeSize(int route) {
        return routes.get(route).size();
    }

    public int countFilledRoute() {
        int count = 0;
        for (Route route : routes) {
            if (route.size() > 0) {
                count++;
            }
        }
        return count;
    }

    public int countAllAppointment() {
        int count = 0;
        for (Route route : routes) {
            count += route.size();
        }
        return count;
    }

    public Appointment getNode(int route, int position) {
        return routes.get(route).get(position);
    }

    public Solution clone() {
        Solution clone = new SolutionImpl();
        clone.setOF(of);
        clone.setRoutes(cloneRoutes());
        return clone;
    }

    /**
     * Internal method for cloning the list of routes
     *
     * @return a hard copy of the list of routes
     */
    private List<AdvancedRoute> cloneRoutes() {
        List<AdvancedRoute> clone = new ArrayList<AdvancedRoute>();
        for (AdvancedRoute r : routes)
            clone.add(r.clone());
        return clone;
    }

    @Override
    public boolean equals(Object o) {
        if (!o.getClass().equals(getClass()))
            return false;

        Solution solution = (Solution) o;
        List<AdvancedRoute> solutionRoutes = solution.getRoutes();

        //If the solutions don't have the same amount of routes, then they are not equal
        if (solutionRoutes.size() != routes.size())
            return false;

        //If any of the routes is not contained in the given solution, then the solutions are not equal
        //this implies that a solution does not contain routes equal to one another
        for (Route r : routes) {
            if (!solutionRoutes.contains(r))
                return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hashCode = 0;

        for (Route r : routes) {
            hashCode += r.hashCode();
        }

        return hashCode;
    }

    @Override
    public String toString() {

        StringBuilder solutionStrBuilder = new StringBuilder();

        int routeId = 0;
        int interventionCount = 0;
        solutionStrBuilder.append("{\n");
        for (AdvancedRoute ar : routes) {

            interventionCount += ar.getRoute().size();
            Technician tech = ar.getTechnician();

            solutionStrBuilder.append("\tTech : " + (routeId++) + "[" + tech.getStartTimeMorning() + ";" + tech.getEndTimeAfternoon());
            solutionStrBuilder.append("] - Lunch : [" + tech.getEndTimeMorning() + ";" + tech.getStartTimeAfternoon());
            solutionStrBuilder.append("] - Skill : " + tech.getSkill().name() + " - Cost : " + ar.getCost() + " - Appointments : " + ar.size());
            solutionStrBuilder.append("\n\t\tDepot => ");

            int appointmentIdInRoute = 0;
            for (Appointment a : ar.getRoute()) {
                int time = ar.getAppointmentStartTime(appointmentIdInRoute++);
                solutionStrBuilder.append(a.getId() + " (@ " + time + ") -> ");
            }

            solutionStrBuilder.append("Depot\n");

        }
        solutionStrBuilder.append("} with " + interventionCount + " interventions\n");

        return solutionStrBuilder.toString();
    }
}
