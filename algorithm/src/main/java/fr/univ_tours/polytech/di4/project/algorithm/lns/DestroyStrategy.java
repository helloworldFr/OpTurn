package fr.univ_tours.polytech.di4.project.algorithm.lns;

import fr.univ_tours.polytech.di4.project.algorithm.solution.Solution;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;

import java.util.List;

/**
 * Strategy pattern for the destroy heuristics for the Large Neighborhood Search
 * Possible strategies:
 * - random nodes on random routes
 * - destroy nodes from one random route
 */
public interface DestroyStrategy {
    /**
     * Partially destroys (removes a subset of appointments from) a solution
     *
     * @param solution the solution to destroy
     * @param removed  an empty list that will contain the list of removed appointments at the end of execution
     * @param param    variable meaning depending on the implemented heuristic
     */
    Solution destroy(Solution solution, List<Appointment> removed, int param);
}
