package fr.univ_tours.polytech.di4.project.algorithm.heuristic;

import fr.univ_tours.polytech.di4.project.algorithm.solution.Solution;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;

import java.util.List;

/**
 * Created by Theo on 21/04/2016.
 * Interface that defines the main method used to build a solution given a set of unrouted appointments
 */
public interface SolutionBuilder {

    /**
     * Builds and returns a feasible and complete solution
     *
     * @return A feasible and complete solution
     */
    Solution buildSolution(List<Appointment> unroutedAppointments);

}
