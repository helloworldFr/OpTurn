package fr.univ_tours.polytech.di4.project.algorithm.lns;

import fr.univ_tours.polytech.di4.project.algorithm.solution.AdvancedRoute;
import fr.univ_tours.polytech.di4.project.algorithm.solution.Solution;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;

import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Repair heuristic that sequentially inserts all the removed nodes back into the destroyed solution, starting from
 * the removed appointment at <code>startPosition</code>.
 * For each removed appointment, it will check EVERY possible insertion position and insert it at the best one.
 * Very expansive heuristic.
 */
public class RepairBestInsertion implements RepairStrategy {
    private Random random = new Random();

    public Solution repair(Solution solution, List<Appointment> removed, int startPosition) {

        Solution newSolution = solution.clone();
        int bestRoute, bestPosition;
        double cost, bestCost;
        AdvancedRoute route;
        Appointment appointment;

        Collections.rotate(removed, startPosition);

        // for each removed appointment
        for (int a = removed.size() - 1; a >= 0; a--) {
            appointment = removed.get(a);
            bestCost = Double.MAX_VALUE;
            bestRoute = -1;
            bestPosition = -1;

            // for each route
            for (int r = 0; r < newSolution.routeCount(); r++) {
                route = newSolution.getRoute(r);
                // for each position in the route.
                //TODO optimise when we need a new route
                for (int i = 0; i <= route.size(); i++) {
                    if (route.isInsertionPossible(appointment, i)) // if a suitable position was found
                    {
                        cost = route.roughInsertCost(appointment, i);
                        if (cost < bestCost) { // if the insertion position is better
                            bestCost = cost;
                            bestPosition = i;
                            bestRoute = r;
                        }
                    }
                }
            }

            // if a suitable position was found
            if (bestPosition >= 0) {
                // insert the appointment
                newSolution.insertNode(appointment, bestRoute, bestPosition); // insert appointment
                removed.remove(a);
            } else {
                throw new IllegalStateException("Appointment did not find a suitable position");
            }
        }

        for (AdvancedRoute r : newSolution.getRoutes()) {
            r.updateCost();
        }
        return newSolution;
    }
}
