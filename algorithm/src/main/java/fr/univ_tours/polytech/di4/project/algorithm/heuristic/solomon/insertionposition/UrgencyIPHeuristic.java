package fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.insertionposition;

import fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.BasicInsertionOffsets;
import fr.univ_tours.polytech.di4.project.algorithm.solution.AdvancedRoute;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;
import fr.univ_tours.polytech.di4.project.data.instance.InstanceDistanceMatrix;

/**
 * Created by Theo on 28/04/2016.
 * Heuristic that should be used with the UrgencyASHeuristic
 * The main difference is that in this heuristic we also take into account
 */
public class UrgencyIPHeuristic extends BasicIPHeuristic {

    private double urgencyFactor;

    /**
     * Constructor of the UrgencyIPHeuristic class
     *
     * @param distanceMatrix The distance matrix
     */
    public UrgencyIPHeuristic(InstanceDistanceMatrix distanceMatrix) {
        super(distanceMatrix);
    }

    /**
     * Sets the urgency factor used to calculate the costs
     * If the given parameter is greater than 1 it will be set to 1
     * If the given parameter is smaller than 0 it will be set to 0
     * if the urgencyFactor + distanceFactor + timeFactor is not equal to 1, then
     * distanceFactor and timeFactor will be modified accordingly
     *
     * @param urgencyFactor The new urgency factor
     */
    public void setUrgencyFactor(double urgencyFactor) {

        if (urgencyFactor < 0) {
            urgencyFactor = 0;
        }
        if (urgencyFactor > 1) {
            urgencyFactor = 1;
        }

        this.urgencyFactor = urgencyFactor;
        if (urgencyFactor + distanceFactor + timeFactor != 1) {
            double remaining = 1 - urgencyFactor;
            distanceFactor *= remaining;
            timeFactor *= remaining;
        }
    }

    /**
     * This implementation is similar to the BasicIPHeuristic's, except this one also takes into account the urgency
     *
     * @param route     The route in which the appointment would be inserted
     * @param insertPos The position at which the appointment would be inserted
     * @param toInsert  The appointment that would be inserted
     * @return The insertion cost
     */
    public double getInsertionCost(AdvancedRoute route, int insertPos, Appointment toInsert) {

        BasicInsertionOffsets.BasicOffsets basicOffsets = basicInsertionOffsets.calculateBasicOffsets(route, insertPos, toInsert);

        double urgencyOffset = toInsert.getEndTime() - basicInsertionOffsets.getMinimumInsertionTime(route, insertPos, toInsert);

        double cost = timeFactor * basicOffsets.getTimeOffset();
        cost += distanceFactor * basicOffsets.getDistanceOffset();
        cost += urgencyFactor * urgencyOffset;

        return cost;
    }
}
