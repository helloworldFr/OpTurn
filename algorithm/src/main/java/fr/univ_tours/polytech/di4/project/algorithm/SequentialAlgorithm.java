package fr.univ_tours.polytech.di4.project.algorithm;

import fr.univ_tours.polytech.di4.project.algorithm.heuristic.SolomonRSBuilder;
import fr.univ_tours.polytech.di4.project.algorithm.solution.AdvancedRoute;
import fr.univ_tours.polytech.di4.project.algorithm.solution.Solution;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;
import fr.univ_tours.polytech.di4.project.data.instance.InstanceDistanceMatrix;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by Theo on 04/05/2016.
 * Class used to test the construction heuristic
 */
public class SequentialAlgorithm implements Algorithm {

    /**
     * Instance used by the SolomonRSBuilder class
     */
    private InstanceDistanceMatrix instance;

    /**
     * Constructor of the SequentialAlgorithm class
     *
     * @param instance The instance that will be used by the SolomonRSBuilder
     */
    public SequentialAlgorithm(InstanceDistanceMatrix instance) {
        this.instance = instance;
    }

    /**
     * This method simply runs a few times the buildSolution method of the SolomonRSBuilder class
     *
     * @return The best solution it created from a heuristic
     */
    public Solution run(Properties properties) {

        long start = System.currentTimeMillis();

        SolomonRSBuilder solomonRSBuilder = new SolomonRSBuilder(instance);

        List<Appointment> appointments = instance.getAppointmentManager().getAppointments();

        Solution solution = null;

        int minRoute = Integer.MAX_VALUE;
        Solution minRouteSolution = null;

        int countErrors = 0;

        for (int i = 0; i < Integer.parseInt(properties.getProperty("HEURISTIC_ITERATION")); i++) {
            List<Appointment> unroutedAppointments = new ArrayList<Appointment>();
            unroutedAppointments.addAll(appointments);

            try {
                solution = solomonRSBuilder.buildSolution(unroutedAppointments);
                List<AdvancedRoute> routes = solution.getRoutes();
                int routeCount = 0;

                for (AdvancedRoute ar : routes) {
                    if (ar.size() > 0)
                        routeCount++;
                }

                if (routeCount < minRoute) {
                    minRoute = routeCount;
                    minRouteSolution = solution;
                }
            } catch (IllegalStateException e) {
                countErrors++;
            }

            //System.out.println("Solution OF : " + solution.getOF() + " | " + routeCount + " routes");
        }
        if (countErrors > 0) {
            System.err.println(countErrors + " solution creation failed / " + Integer.parseInt(properties.getProperty("HEURISTIC_ITERATION")));
        }
        //System.out.println("Result found in " + (System.currentTimeMillis() - start) + "ms !");

        return minRouteSolution;
    }
}
