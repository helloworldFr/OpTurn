package fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.appointmentselection;

import fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.BasicInsertionOffsets;
import fr.univ_tours.polytech.di4.project.algorithm.solution.AdvancedRoute;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;
import fr.univ_tours.polytech.di4.project.data.instance.InstanceDistanceMatrix;

/**
 * Created by Theo on 28/04/2016.
 * This class is a type of AppointmentSelectionHeuristic that tries to minimize the distance and time it takes to
 * complete the route when adding the given appointment (See Solomon, 1987)
 */
public class MinimumCostASHeuristic extends AppointmentSelectionHeuristic {

    /* The BasicInsertionOffsets instance to use to calculate offset costs on adding an appointment to a route */
    protected BasicInsertionOffsets basicInsertionOffsets;

    /* The distance factor used to calculate this heuristic */
    protected double distanceFactor;

    /* The time factor used to calculate this heuristic */
    protected double timeFactor;

    /**
     * MinimumCostASHeuristic constructor
     * Instanciates a BasicInsertionOffsets using the given distance matrix
     *
     * @param distanceMatrix The distance matrix used by the BasicInsertionOffsets instance
     */
    public MinimumCostASHeuristic(InstanceDistanceMatrix distanceMatrix) {
        basicInsertionOffsets = new BasicInsertionOffsets(distanceMatrix);
    }

    /**
     * Calculates the cost associated with the time and distance added by inserting the given appointment
     *
     * @param r         The route in which the appointment would be inserted
     * @param insertPos The position at which the appointment would be inserted
     * @param toInsert  The appointment that would be inserted
     * @return The calculated cost
     */
    public double appointmentSelectionCost(AdvancedRoute r, int insertPos, Appointment toInsert) {

        BasicInsertionOffsets.BasicOffsets basicOffsets = basicInsertionOffsets.calculateBasicOffsets(r, insertPos, toInsert);

        return timeFactor * basicOffsets.getTimeOffset() + distanceFactor * basicOffsets.getDistanceOffset();
    }

    /**
     * Sets the time factor
     * If the given time factor is greater than 1 then set it to 1
     * If the given time factor is less than 0 then set it to 0
     * Sets distanceFactor to equal 1 - timeFactor
     *
     * @param timeFactor
     */
    public void setTimeFactor(double timeFactor) {

        if (timeFactor < 0) {
            timeFactor = 0;
        }

        if (timeFactor > 1) {
            timeFactor = 1;
        }

        this.timeFactor = timeFactor;
        distanceFactor = 1 - timeFactor;
    }

    /**
     * Sets the distance factor
     * If the given distance factor is greater than 1 then set it to 1
     * If the given distance factor is less than 0 then set it to 0
     * Sets timeFactor to equal 1 - distanceFactor
     *
     * @param distanceFactor
     */
    public void setDistanceFactor(double distanceFactor) {

        if (distanceFactor < 0) {
            distanceFactor = 0;
        }

        if (distanceFactor > 1) {
            distanceFactor = 1;
        }

        this.distanceFactor = distanceFactor;
        timeFactor = 1 - distanceFactor;
    }
}
