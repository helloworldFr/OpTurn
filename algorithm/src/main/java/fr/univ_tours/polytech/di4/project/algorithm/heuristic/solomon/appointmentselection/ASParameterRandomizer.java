package fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.appointmentselection;

import java.util.Random;

/**
 * Created by Gilbert on 07/05/2016.
 * This class is used to give random parameters to a given AS heuristic
 */
public class ASParameterRandomizer {

    /* Random object used to randomize given heuristics */
    Random randomGen;

    /**
     * ASParameterRandomizer constructor
     * Instanciates the Random class
     */
    public ASParameterRandomizer() {
        randomGen = new Random();
    }

    /**
     * Given an AS heuristic, this method will give it random parameters depending on the type of AS heuristic it is
     *
     * @param heuristic The heuristic to randomize
     * @return The heuristic once randomized
     */
    public AppointmentSelectionHeuristic randomize(AppointmentSelectionHeuristic heuristic) {

        if (heuristic instanceof MinimumCostASHeuristic) {
            heuristic = randomize((MinimumCostASHeuristic) heuristic);
        } else if (heuristic instanceof RouteKeepingASHeuristic) {
            heuristic = randomize((RouteKeepingASHeuristic) heuristic);
        }

        return heuristic;
    }

    /**
     * Sets the time and distance factor of the given MinimumCostASHeuristic object
     * Those factor will have a value between 0 and 1
     *
     * @param heuristic The heuristic to randomized
     * @return The heuristic once it has been randomized
     */
    private AppointmentSelectionHeuristic randomize(MinimumCostASHeuristic heuristic) {

        double timeFactor = randomGen.nextDouble();

        heuristic.setTimeFactor(timeFactor);
        heuristic.setDistanceFactor(1 - timeFactor);

        return heuristic;
    }

    /**
     * Sets the route creation factor of the given RouteKeepingASHeuristic object
     * This factor will have a value between 0 and 2...
     *
     * @param heuristic The heuristic to randomized
     * @return The heuristic once it has been randomized
     */
    private AppointmentSelectionHeuristic randomize(RouteKeepingASHeuristic heuristic) {

        double routeCreationFactor = randomGen.nextDouble() + randomGen.nextInt(2);

        heuristic.setRouteCreationFactor(routeCreationFactor);

        return heuristic;
    }

}
