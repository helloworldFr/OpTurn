package fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon;

import fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.appointmentselection.*;
import fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.insertionposition.BasicIPHeuristic;
import fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.insertionposition.IPParameterRandomizer;
import fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.insertionposition.InsertionPositionHeuristic;
import fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.insertionposition.UrgencyIPHeuristic;
import fr.univ_tours.polytech.di4.project.algorithm.solution.AdvancedRoute;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;
import fr.univ_tours.polytech.di4.project.data.cost.ConstantManager;
import fr.univ_tours.polytech.di4.project.data.instance.InstanceDistanceMatrix;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Theo on 21/04/2016.
 * This class contains a set of costs functions as described by the Solomon paper (1987), and are used
 * for the insertion heuristic used when creating an initial solution
 * This class has been created to work with the Route and the Appointment class
 * It uses an InstanceDistanceMatrix to fetch the distance and time between two appointments
 */
public class SolomonCosts {

    /* A map containing all the available insertion position heuristic */
    private Map<String, InsertionPositionHeuristic> insertionPositionHeuristics = new HashMap<String, InsertionPositionHeuristic>();
    /* A map containing all the available appointment selection heuristic */
    private Map<String, AppointmentSelectionHeuristic> appointmentSelectionHeuristics = new HashMap<String, AppointmentSelectionHeuristic>();

    /* A map containing the dependencies an AS heuristic might have for an IP heuristic */
    private Map<String, String> dependencyRules = new HashMap<String, String>();

    /* The IP heuristic to use when calculating costs */
    private InsertionPositionHeuristic currentIPHeuristic;
    /* The AS heuristic to use when calculating costs */
    private AppointmentSelectionHeuristic currentASHeuristic;

    /**
     * Constructor of the SolomonCosts class
     * Instantiates various AS and IP heuristics
     * Sets the current heuristics to use
     * Creates dependency rules between heuristics
     *
     * @param distanceMatrix The distance matrix used by the heuristics
     */
    public SolomonCosts(ConstantManager constantManager, InstanceDistanceMatrix distanceMatrix) {

        insertionPositionHeuristics.put("IP_BASIC", new BasicIPHeuristic(distanceMatrix));
        insertionPositionHeuristics.put("IP_URGENCY", new UrgencyIPHeuristic(distanceMatrix));

        appointmentSelectionHeuristics.put("AS_KEEP_ROUTE", new RouteKeepingASHeuristic(distanceMatrix));
        appointmentSelectionHeuristics.put("AS_MIN_COST", new MinimumCostASHeuristic(distanceMatrix));
        appointmentSelectionHeuristics.put("AS_URGENCY", new UrgencyASHeuristic());

        setInsertionPositionHeuristic("IP_BASIC");
        setAppointmentSelectionHeuristic("AS_KEEP_ROUTE");

        dependencyRules.put("AS_MIN_COST", "IP_BASIC");
        dependencyRules.put("AS_KEEP_ROUTE", "IP_BASIC");
        dependencyRules.put("AS_URGENCY", "IP_URGENCY");
    }

    /**
     * Returns the keys of the available IP heuristics
     *
     * @return A set of IP heuristic keys
     */
    public Set<String> getInsertionPositionHeuristics() {
        return insertionPositionHeuristics.keySet();
    }

    /**
     * Returns the keys of the available AS heuristics
     *
     * @return A set of AS heuristic keys
     */
    public Set<String> getAppointmentSelectionHeuristics() {
        return appointmentSelectionHeuristics.keySet();
    }

    /**
     * Sets the IP heuristic to use
     *
     * @param key The key of the IP heuristic to use
     * @return True if the IP heuristic exists and could be set
     * False otherwise
     */
    public boolean setInsertionPositionHeuristic(String key) {

        if (!insertionPositionHeuristics.containsKey(key)) {
            return false;
        }

        //If the current IP heuristic is a dependency, then do not change it
        if (currentASHeuristic != null) {
            for (Map.Entry entry : appointmentSelectionHeuristics.entrySet()) {
                if (entry.getValue().equals(currentASHeuristic)) {
                    if (dependencyRules.containsKey(entry.getKey())) {
                        return false;
                    }
                }
            }
        }

        currentIPHeuristic = insertionPositionHeuristics.get(key);
        return true;
    }

    /**
     * Sets the AS heuristic to use
     * An AS heuristic might need an IP heuristic to calculate its cost, so if there is a dependency
     * between an AS heuristic and an IP heuristic, the AS heuristic will use for IP heuristic
     * the depended IP heuristic. Otherwise it will use for IP heuristic the current IP heuristic
     *
     * @param key The key of the AS heuristic to use
     * @return True if the IP heuristic exists and could be set
     * False otherwise
     */
    public boolean setAppointmentSelectionHeuristic(String key) {
        if (!appointmentSelectionHeuristics.containsKey(key)) {
            return false;
        }

        currentASHeuristic = appointmentSelectionHeuristics.get(key);

        if (dependencyRules.containsKey(key)) {
            if (!insertionPositionHeuristics.containsKey(dependencyRules.get(key))) {
                currentASHeuristic.setInsertionPositionHeuristic(currentIPHeuristic);
                return false;
            }

            InsertionPositionHeuristic dependedIPHeuristic = insertionPositionHeuristics.get(dependencyRules.get(key));
            currentASHeuristic.setInsertionPositionHeuristic(dependedIPHeuristic);
            currentIPHeuristic = dependedIPHeuristic;
        } else {
            currentASHeuristic.setInsertionPositionHeuristic(currentIPHeuristic);
        }
        return true;
    }

    /**
     * Randomizes the current heuristics using the given randomizers
     *
     * @param ipParameterRandomizer The randomizer used with the IP heuristic
     * @param asParameterRandomizer The randomizer used with the AS heuristic
     */
    public void randomizeHeuristics(IPParameterRandomizer ipParameterRandomizer, ASParameterRandomizer asParameterRandomizer) {
        currentIPHeuristic = ipParameterRandomizer.randomize(currentIPHeuristic);
        currentASHeuristic = asParameterRandomizer.randomize(currentASHeuristic);
    }

    /**
     * Returns the current insertion position heuristic
     *
     * @return The current IP heuristic
     */
    public InsertionPositionHeuristic getCurrentInsertionPositionHeuristic() {
        return currentIPHeuristic;
    }

    /**
     * Returns the current appointment selection heuristic
     *
     * @return The current AS heuristic
     */
    public AppointmentSelectionHeuristic getCurrentAppointmentSelectionHeuristic() {
        return currentASHeuristic;
    }

    /**
     * Calls the current inserting position selection heuristic to calculate its cost
     *
     * @param r         The route in which to insert the appointment
     * @param insertPos The position after which to insert the appointment
     * @param toInsert  The appointment to insert
     * @return The cost of inserting toInsert at the given position
     */
    public double insertPositionSelectionCost(AdvancedRoute r, int insertPos, Appointment toInsert) {
        return currentIPHeuristic.getInsertionCost(r, insertPos, toInsert);
    }

    /**
     * Calls the current appointment selection heuristic to calculate its cost
     *
     * @param r         The route in which to insert the appointment
     * @param insertPos The position after which to insert the appointment
     * @param toInsert  The appointment to insert
     * @return The cost of inserting the given appointment after insertPos
     */
    public double appointmentSelectionCost(AdvancedRoute r, int insertPos, Appointment toInsert) {
        return currentASHeuristic.appointmentSelectionCost(r, insertPos, toInsert);
    }
}