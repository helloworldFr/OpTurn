/* Log of changes:
 * Mar 24, 2012   Solution implemented
 */

/* To Do List:
 * 
 */
package fr.univ_tours.polytech.di4.project.algorithm.solution;

import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;

import java.io.Serializable;
import java.util.List;

/**
 * Defines an interface of VRP solutions
 *
 * @author Jorge E. Mendoza (dev@jorge-mendoza.com)
 * @author Theo Menard
 * @since Mar 24, 2012
 */
public interface Solution extends Cloneable, Serializable {

    /**
     * @return the objective function of the solution
     */
    double getOF();

    /**
     * @param of the objective function to set
     */
    void setOF(final double of);

    /**
     * Updates the objective function value to be equal to the sum of the cost of each route.
     * Does not update route costs.
     */
    void updateOF();

    /**
     * Recalculate the cost of each route, then updates the objective function value
     * to be equal to the sum of the cost of each route
     */
    void updateAllOF();

    /**
     * @return a clone of the routes
     */
    List<AdvancedRoute> getRoutes();

    /**
     * Get the route at position <code>route</code>
     *
     * @param route
     * @return Route
     */
    AdvancedRoute getRoute(int route);

    /**
     * Remove a route
     *
     * @param route
     */
    void removeRoute(int route);

    /**
     * @param routes
     */
    void setRoutes(List<AdvancedRoute> routes);

    /**
     * Add a new route to the solution
     *
     * @param route the route
     */
    void addRoute(AdvancedRoute route);

    /**
     * Remove a route from the solution
     *
     * @param route the route
     */
    void removeRoute(AdvancedRoute route);

    /**
     * Inserts <code>node</code> in position <code>position</code> of route <code>route</code></br>
     * <p>
     * <strong>Examples</strong><br>
     * <p>
     * Assume the current solution <code>s</code> has two routes <code>{0,3,4,5,0}</code> and <code>{0,2,1,0}</code>.</br>
     * <p>
     * <code>insertNode(6,1,0)</code> leads to <code>s={{0,3,4,5,0},{6,0,2,1,0}}</code></br>
     * <code>insertNode(6,1,1)</code> leads to <code>s={{0,3,4,5,0},{0,6,2,1,0}}</code></br>
     * <code>insertNode(6,1,3)</code> leads to <code>s={{0,3,4,5,0},{0,2,1,6,0}}</code></br>
     * <code>insertNode(6,1,4)</code> leads to <code>s={{0,3,4,5,0},{0,2,1,0,6}}</code></br>
     * <p>
     * Note that for the sake of flexibility this method does not check semantic constraints such as: the first and last node in the route should be the same, or
     * a node cannot be visited more than once by a route. Client classes are responsible for controlling these constraints depending on the VRP in hand.
     *
     * @param appointment the ID of the node to insertNode
     * @param route       the route
     * @param position    the position in route <code>route</code>
     */
    void insertNode(Appointment appointment, int route, int position);

    /**
     * Removes the node in position <code>position</code> of route <code>route</code>
     * <p>
     * <strong>Examples</strong><br>
     * <p>
     * Assume the current solution <code>s</code> has two routes <code>{0,3,4,5,0}</code> and <code>{0,2,1,0}</code>.</br>
     * <p>
     * <code>removeNode(1,0)</code> leads to <code>s={{0,3,4,5,0},{2,1,0}}</code></br>
     * <code>insertNode(1,1)</code> leads to <code>s={{0,3,4,5,0},{0,1,0}}</code></br>
     * <code>insertNode(1,3)</code> leads to <code>s={{0,3,4,5,0},{0,2,1}}</code></br>
     * <p>
     * Note that for the sake of flexibility this method does not check semantic constraints such as: the first and last node in the route should be the same, or
     * a node cannot be visited more than once by a route. Client classes are responsible for controlling these constraints depending on the VRP in hand.
     *
     * @param route    the route
     * @param position the position in route <code>r</code>
     * @return the ID of the removed node
     */
    Appointment removeNode(int route, int position);

    /**
     * Get the cost of the route
     *
     * @param route
     * @return double
     */
    double getRouteCost(int route);

    /**
     * Set the cost of a route
     *
     * @param cost  the cost
     * @param route the route
     */
    void setRouteCost(int route, double cost);

    /**
     * Get the number of routes in the solution
     *
     * @return int
     */
    int routeCount();

    /**
     * Get the number of nodes in the specified route
     *
     * @param route the route
     * @return int
     */
    int routeSize(int route);

    /**
     * Get the number of routes not empty
     *
     * @return int
     */
    int countFilledRoute();

    /**
     * Get the number of appointment i route
     *
     * @return int
     */
    int countAllAppointment();

    /**
     * Get the node in position <code>position</code> of route <code>route</code>
     *
     * @param route
     * @param position
     * @return
     */
    Appointment getNode(int route, int position);

    /**
     * @return a dept copy of the solution
     */
    Solution clone();

    /**
     * Compares the solution with a given object. Returns true if both are solutions composed of the same routes
     * Equals is needed to be reimplemented because we need to be able to check that we do not have twice the same
     * solution to optimize computational costs
     *
     * @param o The object to compare the solution to
     * @return True if the object is a solution that contains routes equal to the current one
     * False otherwise
     */
    boolean equals(Object o);

    /**
     * The hashCode method should be consistent with the equals method (two objects that are equal should return the
     * same hashcode)
     * For this reason it needs to be reimplemented when implementing a solution
     *
     * @return The hashcode of the given solution
     */
    int hashCode();

}
