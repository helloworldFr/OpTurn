package fr.univ_tours.polytech.di4.project.algorithm;

import fr.univ_tours.polytech.di4.project.algorithm.solution.Solution;

import java.util.Properties;

/**
 * Defines the interface to optimization algorithms
 */
public interface Algorithm {
    /**
     * Run the algorithm
     *
     * @return A solution
     */
    Solution run(Properties properties);
}
