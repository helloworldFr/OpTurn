package fr.univ_tours.polytech.di4.project.algorithm.heuristic;

import fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.SolomonCosts;
import fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.SolomonHeuristicUtils;
import fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.appointmentselection.ASParameterRandomizer;
import fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.insertionposition.IPParameterRandomizer;
import fr.univ_tours.polytech.di4.project.algorithm.solution.AdvancedRoute;
import fr.univ_tours.polytech.di4.project.algorithm.solution.Solution;
import fr.univ_tours.polytech.di4.project.algorithm.solution.SolutionImpl;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;
import fr.univ_tours.polytech.di4.project.data.instance.InstanceDistanceMatrix;
import fr.univ_tours.polytech.di4.project.data.technician.Technician;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Theo on 03/05/2016.
 * This class is an implementation of the RandomizedSolutionBuilder
 * It uses the SolomonCosts and SolomonHeuristicUtils to create a solution
 * using a (if no dependency between heuristics) random pair of heuristic with random parameters
 */
public class SolomonRSBuilder implements RandomizedSolutionBuilder {

    /* A list that will contain the keys of all of the available appointment selection heuristic */
    private List<String> ASHeuristicsKeys = new ArrayList<String>();
    /* A list that will contain the keys of all of the available insertion position heuristic */
    private List<String> IPHeuristicsKeys = new ArrayList<String>();

    /* The SolomonCosts instance to use to use the heuristics */
    private SolomonCosts solomonCosts;

    /* The instance distance matrix to use with the heuristics */
    private InstanceDistanceMatrix instance;

    /* The ASParameterRandomizer to use to randomize an AS heuristic */
    private ASParameterRandomizer asParameterRandomizer;

    /* The IPParameterRandomizer to use to randomize an IP heuristic */
    private IPParameterRandomizer ipParameterRandomizer;

    /* The random number generator used to select random heuristic, if there is no dependency with a selected heuristic */
    private Random randomGenerator;

    /**
     * Constructor of the SolomonRSBuilder class
     * It instanciates the SolomonCosts class
     * It adds the available heuristics' keys to the ASHeuristicKeys and IPHeuristicKeys lists
     * It also instanciates the IPParameterRandomizer and the ASParameterRandomizer
     *
     * @param instance The instance to use
     */
    public SolomonRSBuilder(InstanceDistanceMatrix instance) {

        this.instance = instance;

        randomGenerator = new Random();

        solomonCosts = new SolomonCosts(instance.getConstantManager(), instance);

        ASHeuristicsKeys.addAll(solomonCosts.getAppointmentSelectionHeuristics());
        IPHeuristicsKeys.addAll(solomonCosts.getInsertionPositionHeuristics());

        ipParameterRandomizer = new IPParameterRandomizer();
        asParameterRandomizer = new ASParameterRandomizer();
    }

    /**
     * Selects random heuristic with random parameters and builds a solution using them
     *
     * @param unroutedAppointments The appointments to route
     * @return A feasible Solution containing the unrouted appointments
     * If any appointment could not be put into the solution, throws an IllegalStateException
     */
    public Solution buildSolution(List<Appointment> unroutedAppointments) {

        int ASRandomIndex = randomGenerator.nextInt(ASHeuristicsKeys.size());
        int IPRandomIndex = randomGenerator.nextInt(IPHeuristicsKeys.size());

        solomonCosts.setInsertionPositionHeuristic(IPHeuristicsKeys.get(IPRandomIndex));
        solomonCosts.setAppointmentSelectionHeuristic(ASHeuristicsKeys.get(ASRandomIndex));

        solomonCosts.randomizeHeuristics(ipParameterRandomizer, asParameterRandomizer);

        Solution solution = new SolutionImpl();
        AdvancedRoute currentRoute = null;
        int currentRouteIndex = 0;

        for (Technician tech : instance.getTechnicianManager().getTechnicians()) {
            AdvancedRoute route = new AdvancedRoute(instance);
            route.setTechnician(tech);
            solution.addRoute(route);
        }

        SolomonHeuristicUtils solomonHeuristicUtils = new SolomonHeuristicUtils(solomonCosts);
        Map<Appointment, Integer> insertionPositions;

        while (!unroutedAppointments.isEmpty() && currentRouteIndex < solution.getRoutes().size()) {

            currentRoute = solution.getRoute(currentRouteIndex);

            insertionPositions = solomonHeuristicUtils.getBestInsertPositions(currentRoute, unroutedAppointments);

            if (insertionPositions.isEmpty()) {
                currentRouteIndex++;
                currentRoute.updateCost();
            } else {
                Appointment toInsert = solomonHeuristicUtils.getBestAppointmentToInsert(currentRoute, insertionPositions);

                if (toInsert != null) {
                    AdvancedRoute clone = currentRoute.clone();
                    currentRoute.insert(toInsert, insertionPositions.get(toInsert));
                    unroutedAppointments.remove(toInsert);
                }
            }
        }

        if (!unroutedAppointments.isEmpty()) {
            throw new IllegalStateException("Some appointments could not be inserted in any route ");
        }

        //We update the last currentRoute's cost
        if (currentRoute != null) {
            currentRoute.updateCost();
        }

        solution.updateOF();

        return solution;
    }
}
