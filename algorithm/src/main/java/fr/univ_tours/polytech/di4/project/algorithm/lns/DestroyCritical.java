package fr.univ_tours.polytech.di4.project.algorithm.lns;

import fr.univ_tours.polytech.di4.project.algorithm.solution.AdvancedRoute;
import fr.univ_tours.polytech.di4.project.algorithm.solution.Solution;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;

import java.util.List;
import java.util.TreeSet;

/**
 * Destroy heuristic that removes nodes whose removal would save as much as possible
 */
public class DestroyCritical implements DestroyStrategy {

    private class NodePosition implements Comparable<NodePosition> {
        int route = 0;
        int position = 0;
        double costOfRemoval = 0;

        public NodePosition(int route, int position, double cost) {
            this.route = route;
            this.position = position;
            this.costOfRemoval = cost;
        }

        public int compareTo(NodePosition o) {
            return (int) (this.costOfRemoval - o.costOfRemoval);
        }
    }

    public Solution destroy(Solution solution, List<Appointment> removed, int amount) {

        Solution newSolution = solution.clone();
        double costOfRemoval;
        AdvancedRoute route;
        TreeSet<NodePosition> criticalNodes = new TreeSet<NodePosition>();
        criticalNodes.add(new NodePosition(-1, -1, Double.MAX_VALUE)); //placeholder node

        for (int r = 0; r < newSolution.routeCount(); r++) { // for each route
            route = newSolution.getRoute(r);
            for (int p = 0; p < route.size(); p++) { // for each position in the route
                costOfRemoval = route.roughRemoveCost(p);

                if (costOfRemoval < criticalNodes.last().costOfRemoval) { // if the cost is lower than the highest cost
                    criticalNodes.add(new NodePosition(r, p, costOfRemoval)); // save it
                    if (criticalNodes.size() > amount) {
                        criticalNodes.remove(criticalNodes.last()); // remove extra nodes
                    }
                }
            }
        }
        TreeSet<NodePosition> toBeRemoved = new TreeSet<NodePosition>();

        for (NodePosition n : criticalNodes) {
            if (n.route >= 0) {
                // since cost is irrelevant now, use it to sort the new set so that higher position nodes appear first.
                // disgusting way of doing it though
                NodePosition tmp = new NodePosition(n.route, n.position, n.costOfRemoval);
                tmp.costOfRemoval = -(tmp.position);
                toBeRemoved.add(tmp);
            } else { // the placeholder node
                criticalNodes.remove(n);
            }
        }

        for (NodePosition n : toBeRemoved) {
            removed.add(newSolution.getNode(n.route, n.position));
            newSolution.removeNode(n.route, n.position);
        }


        return newSolution;
    }
}
