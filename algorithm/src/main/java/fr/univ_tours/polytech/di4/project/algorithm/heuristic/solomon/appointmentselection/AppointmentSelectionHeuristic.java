package fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.appointmentselection;

import fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.insertionposition.InsertionPositionHeuristic;
import fr.univ_tours.polytech.di4.project.algorithm.solution.AdvancedRoute;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;

/**
 * Created by Theo on 27/04/2016.
 * A heuristic that is used to select an appointment to add to the current route among a set of unrouted appointments
 * that can feasibly be inserted
 */
public abstract class AppointmentSelectionHeuristic {

    /**
     * The Insertion Position heuristic to use when calculating the appointment selection cost
     */
    protected InsertionPositionHeuristic insertionPositionHeuristic;

    /**
     * Returns a cost associated with the insertion of an appointment on a given route in a given position
     * This method should be used to select which appointment should be inserted in the current route
     *
     * @param r         The route in which the appointment would be inserted
     * @param insertPos The position at which the appointment would be inserted
     * @param toInsert  The appointment that would be inserted
     * @return The calculated cost
     */
    public abstract double appointmentSelectionCost(AdvancedRoute r, int insertPos, Appointment toInsert);

    /**
     * Compare two costs
     *
     * @param cost1 The first cost to compare
     * @param cost2 The second cost to compare
     * @return -1 if cost1 is better than cost2, 0 if they are equivalent, 1 otherwise
     */
    public int compare(double cost1, double cost2) {
        if (cost1 < cost2)
            return -1;
        if (cost1 == cost2)
            return 0;

        return 1;
    }

    /**
     * Sets the IP heuristic
     *
     * @param insertionPositionHeuristic The IP heuristic to use
     */
    public void setInsertionPositionHeuristic(InsertionPositionHeuristic insertionPositionHeuristic) {
        this.insertionPositionHeuristic = insertionPositionHeuristic;
    }
}
