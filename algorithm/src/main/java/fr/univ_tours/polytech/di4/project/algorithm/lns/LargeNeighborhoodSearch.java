package fr.univ_tours.polytech.di4.project.algorithm.lns;

import fr.univ_tours.polytech.di4.project.algorithm.solution.Solution;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;

/**
 * Created by Théo on 01/05/2016.
 * Implements the Large Neighborhood Search algorithm
 */
public class LargeNeighborhoodSearch {
    private Random random = new Random();

    private DestroyStrategy destroyRandom = new DestroyRandom();//new DestroyCritical();
    private DestroyStrategy destroyCritical = new DestroyCritical();
    private RepairStrategy repair = new RepairBestInsertion();


    /**
     * Runs the LNS algorithm. Will repeatedly destroy and repair a solution, improving it after each step.
     *
     * @param solution the solution to improve
     * @return an improved solution
     */
    public Solution run(Properties properties, Solution solution) {
        Solution destroyedSolution, repairedSolution;
        List<Appointment> removed = new ArrayList<Appointment>();
        int maxIteration = Integer.parseInt(properties.getProperty("LNS_ITERATION"));
        int maxDestroy = Integer.parseInt(properties.getProperty("LNS_MAX_DESTROY"));
        int amount;
        int startPosition;
        int countErrors = 0;

        //cannot allow remove more than the half of the solution
        if (maxDestroy >= solution.countAllAppointment() / 2) {
            maxDestroy = solution.countAllAppointment() / 2;
        }

        for (int i = 0; i < maxIteration; i++) {
            amount = random.nextInt(maxDestroy) + 5;
            DestroyStrategy destroy = (i % 2 == 0) ? destroyRandom : destroyCritical;

            try {
                destroyedSolution = destroy.destroy(solution, removed, amount);
                startPosition = random.nextInt(removed.size() - 1);

                repairedSolution = repair.repair(destroyedSolution, removed, startPosition);
                repairedSolution.updateAllOF();
                if (repairedSolution.getOF() < solution.getOF())
                    solution = repairedSolution;
            } catch (IllegalStateException e) {
                countErrors++;
            } catch (IllegalArgumentException e) {
                countErrors++;
            }
        }

        if (countErrors > 0) {
            System.err.println(countErrors + " LNS errors / " + maxIteration);
        }
        return solution;
    }
}