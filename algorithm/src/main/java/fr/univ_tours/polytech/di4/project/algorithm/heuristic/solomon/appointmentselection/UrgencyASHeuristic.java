package fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.appointmentselection;

import fr.univ_tours.polytech.di4.project.algorithm.solution.AdvancedRoute;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;

/**
 * Created by Theo on 28/04/2016.
 * This class is a type of AppointmentSelectionHeuristic that takes into account the urgency of going to an appointment
 * See Solomon, 1987
 */
public class UrgencyASHeuristic extends AppointmentSelectionHeuristic {

    /**
     * This implementation simply calls the UrgencyIPHeuristic's cost function
     *
     * @param r         The route in which the appointment would be inserted
     * @param insertPos The position at which the appointment would be inserted
     * @param toInsert  The appointment that would be inserted
     * @return The cost returned by the UrgencyIPHeuristic's cost function
     */
    public double appointmentSelectionCost(AdvancedRoute r, int insertPos, Appointment toInsert) {
        return insertionPositionHeuristic.getInsertionCost(r, insertPos, toInsert);
    }
}
