package fr.univ_tours.polytech.di4.project.algorithm.lns;

import fr.univ_tours.polytech.di4.project.algorithm.solution.Solution;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;

import java.util.List;
import java.util.Random;

/**
 * Created by Théo on 06/05/2016.
 */
public class DestroyRoute implements DestroyStrategy {

    private Random random = new Random();

    /**
     * Entirely destroy a route from the solution
     *
     * @param solution the solution to destroy
     * @param removed  an empty list that will contain the list of removed appointments at the end of execution
     * @param route    the position of the route to destroy
     * @return
     */
    public Solution destroy(Solution solution, List<Appointment> removed, int route) {

        Solution newSolution = solution.clone();
        for (int i = newSolution.routeSize(route) - 1; i >= 0; i--) {
            removed.add(newSolution.getNode(route, i));
            newSolution.removeNode(route, i);
        }
        return newSolution;
    }
}
