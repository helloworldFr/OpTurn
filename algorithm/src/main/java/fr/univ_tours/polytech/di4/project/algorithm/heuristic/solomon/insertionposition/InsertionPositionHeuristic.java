package fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.insertionposition;

import fr.univ_tours.polytech.di4.project.algorithm.solution.AdvancedRoute;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;

/**
 * Created by Theo on 27/04/2016.
 * Interface for c1 (see Solomon 1987 p. 5) which is a cost function used to select a position at which it is best
 * to insert a given appointment in a given route
 */
public abstract class InsertionPositionHeuristic {

    /**
     * distance factor used in the cost function
     */
    protected double distanceFactor = 0.5;

    /**
     * time factor used in the cost function
     */
    protected double timeFactor = 0.5;

    /**
     * Sets the distance factor used by the cost function
     * If the given parameter is greater than 1, it will be set to 1
     * If the given parameter is lower than 0, it will be set to 0
     * timeFactor + distanceFactor should always be equal to 1
     *
     * @param distanceFactor The distance factor
     */
    public void setDistanceFactor(double distanceFactor) {

        if (distanceFactor > 1)
            distanceFactor = 1;

        if (distanceFactor < 0)
            distanceFactor = 0;

        this.distanceFactor = distanceFactor;
        timeFactor = 1 - timeFactor;
    }

    /**
     * Sets the time factor used by the cost function
     * If the given parameter is greater than 1, it will be set to 1
     * If the given parameter is lower than 0, it will be set to 0
     * timeFactor + distanceFactor should always be equal to 1
     *
     * @param timeFactor
     */
    public void setTimeFactor(double timeFactor) {

        if (timeFactor > 1)
            timeFactor = 1;

        if (timeFactor < 0)
            timeFactor = 0;

        this.timeFactor = timeFactor;
        distanceFactor = 1 - timeFactor;
    }

    /**
     * Returns a cost associated with the insertion of an appointment on a given route in a given position
     * This cost is a linear composition of the changed distance and of the changed length in time of the route
     * This method should be used to select where an appointment should be selected in the current route
     *
     * @param route     The route in which the appointment would be inserted
     * @param insertPos The position at which the appointment would be inserted
     * @param toInsert  The appointment that would be inserted
     * @return The calculated cost
     */
    public abstract double getInsertionCost(AdvancedRoute route, int insertPos, Appointment toInsert);

    /**
     * Compare two costs
     *
     * @param cost1 The first cost to compare
     * @param cost2 The second cost to compare
     * @return -1 if cost1 is better than cost2, 0 if they are equivalent, 1 otherwise
     */
    public int compare(double cost1, double cost2) {
        if (cost1 < cost2)
            return -1;
        if (cost1 == cost2)
            return 0;

        return 1;
    }
}
