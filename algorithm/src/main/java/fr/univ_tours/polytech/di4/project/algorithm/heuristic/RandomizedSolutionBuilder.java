package fr.univ_tours.polytech.di4.project.algorithm.heuristic;

/**
 * Created by Theo on 21/04/2016.
 * Classes extending this Interface should generate solutions randomly
 * That is, calling twice the buildSolution method should not necessarily build the same solution
 */
public interface RandomizedSolutionBuilder extends SolutionBuilder {
}
