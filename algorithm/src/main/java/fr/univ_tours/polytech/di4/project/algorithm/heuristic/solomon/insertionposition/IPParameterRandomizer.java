package fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.insertionposition;

import fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.appointmentselection.UrgencyASHeuristic;

import java.util.Random;

/**
 * Created by Gilbert on 07/05/2016.
 * This class is used to give random parameters to a given IP heuristic
 */
public class IPParameterRandomizer {

    /* Random object used to randomize given heuristics */
    Random randomGen;

    /**
     * IPParameterRandomizer constructor
     * Instanciates the Random class
     */
    public IPParameterRandomizer() {
        randomGen = new Random();
    }

    /**
     * Given an IP heuristic, this method will give it random parameters depending on the type of IP heuristic it is
     *
     * @param heuristic The heuristic to randomize
     * @return The heuristic once randomized
     */
    public InsertionPositionHeuristic randomize(InsertionPositionHeuristic heuristic) {

        double timeFactor = randomGen.nextDouble();

        heuristic.setTimeFactor(timeFactor);
        heuristic.setDistanceFactor(1 - timeFactor);

        if (heuristic.getClass().equals(UrgencyASHeuristic.class)) {
            heuristic = randomize((UrgencyIPHeuristic) heuristic);
        }


        return heuristic;
    }

    /**
     * Sets the time, distance and urgency factor of the given UrgencyIPHeuristic object
     * Those factor will have a value between 0 and 1
     *
     * @param heuristic The heuristic to randomized
     * @return The heuristic once it has been randomized
     */
    private InsertionPositionHeuristic randomize(UrgencyIPHeuristic heuristic) {

        double urgencyFactor = randomGen.nextDouble();

        heuristic.setUrgencyFactor(urgencyFactor);

        return heuristic;
    }
}
