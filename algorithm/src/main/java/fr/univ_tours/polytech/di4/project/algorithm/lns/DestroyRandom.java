package fr.univ_tours.polytech.di4.project.algorithm.lns;

import fr.univ_tours.polytech.di4.project.algorithm.solution.Solution;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;

import java.util.List;
import java.util.Random;

/**
 * Destroy heuristic that removes appointments at random (from a random route at a random position)
 */
public class DestroyRandom implements DestroyStrategy {

    private int route, position;
    private Random random = new Random();

    /**
     * Destroys <code>amount</code> appointments from random routes at a random position
     *
     * @param solution the solution to destroy
     * @param removed  an empty list that will contain the list of removed appointments at the end of execution
     * @param amount   the amount of nodes to remove in total
     * @return
     */
    public Solution destroy(Solution solution, List<Appointment> removed, int amount) {
        Solution newSolution = solution.clone();
        for (int a = 0; a < amount; a++) // remove amount appointments
        {
            do {
                route = random.nextInt(newSolution.routeCount());
            } while (newSolution.routeSize(route) < 2);
            if (newSolution.routeSize(route) == 1) {
                position = 0;
            } else {
                position = random.nextInt(newSolution.routeSize(route) - 1);
            }

            removed.add(newSolution.getNode(route, position));
            newSolution.removeNode(route, position);
        }

        return newSolution;
    }
}
