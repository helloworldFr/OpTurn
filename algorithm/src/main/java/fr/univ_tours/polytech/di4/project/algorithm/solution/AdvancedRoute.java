package fr.univ_tours.polytech.di4.project.algorithm.solution;

import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;
import fr.univ_tours.polytech.di4.project.data.instance.EdgeTravelingInformation;
import fr.univ_tours.polytech.di4.project.data.instance.InstanceDistanceMatrix;
import fr.univ_tours.polytech.di4.project.data.technician.TechnicianSkills;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Théo on 28/04/2016.
 */
public class AdvancedRoute extends Route {

    private transient InstanceDistanceMatrix instance;

    public AdvancedRoute(InstanceDistanceMatrix instance) {
        this.instance = instance;
    }

    /**
     * Inserts a node at a given position of the route. After the insertion, the node previouly in position
     * <code>i</code> is in position <code>i+1</code>. Also updates all the start times of the appointments.
     * This does not check if insertion is possible. Use isInsertionPossible() beforehand.
     *
     * @param appointment the node to insert
     * @param position    the inserting position
     */
    public void insert(Appointment appointment, int position) throws IllegalStateException {
        if (!computeNewStartTimes(appointment, position, true)) {
            throw new IllegalStateException("Could not insert the appointment " + appointment.getId() + " at position " + position);
        }
    }

    /**
     * Calculates if inserting an appointment is possible in this route for a given position.
     * After the insertion, the node previouly in position <code>i</code> is in position <code>i+1</code>.
     * Takes into account the technician skill as well as time constraints.
     *
     * @param appointment the appointment to insert
     * @param position    the position where the appointment will be inserted
     * @return true if the insertion is possible, false otherwise
     */
    public boolean isInsertionPossible(Appointment appointment, int position) {

        return computeNewStartTimes(appointment, position, false);
    }

    /**
     * Computes the new start times that would arise when inserting an appointment at a given position
     * If the insert parameter is set to true, this method will also add to the route the given appointment
     *
     * @param appointment The appointment to insert
     * @param position    The position at which to insert the appointment
     * @param insert      True if the appointment should be added to the route, false otherwise
     * @return True if the appointment can be legally inserted, false otherwise
     */
    private boolean computeNewStartTimes(Appointment appointment, int position, boolean insert) {

        // TECHNICIAN SKILLS
        if (getTechnician().getSkill() != TechnicianSkills.SKILL_2) // if the technician can not do everything
        {
            if (appointment.getSkill() != getTechnician().getSkill())
                return false;
        }


        //At first the newStartTime is equal to the time before to start travelling to the given appointment
        int newStartTime = technician.getStartTimeMorning();
        if (position > 0) {
            newStartTime = getAppointmentEndTime(position - 1);
        }

        //If we are inserting the appointment, we'll need to keep in memory the new start times
        List<Integer> newStartTimes = null;
        int insertedStartTime = 0;

        if (insert) { //The array list is only created when it is needed
            newStartTimes = new ArrayList<Integer>();
        }

        Appointment endNode = appointment;

        boolean isMorning = (newStartTime <= technician.getEndTimeMorning());
        if (!isMorning && newStartTime < technician.getStartTimeAfternoon()) {
            newStartTime = technician.getStartTimeAfternoon();
        }

        //Then for each position after the appointment
        for (int i = position - 1; i < size(); i++) {

            if (i >= position) {
                endNode = get(i);
            }

            int travelTime = getTravelTimeToNextAppointment(i - 1, newStartTime, appointment, position);

            int projectedEndTime = Math.max(newStartTime + travelTime, endNode.getStartTime()) + endNode.getDurationTime();

            //If the appointment cannot be finished after the end of the morning
            if (isMorning && projectedEndTime > technician.getEndTimeMorning()) {
                isMorning = false;

                //If it isn't possible to travel to the next appointment before the end of the morning, do it first thing in the afternoon
                if (newStartTime + travelTime >= technician.getEndTimeMorning()) {
                    travelTime = getTravelTimeToNextAppointment(i - 1, newStartTime, appointment, position);
                    newStartTime = technician.getStartTimeAfternoon() + travelTime;
                } else {//if it is possible to travel to the next appointment before the end of the morning, do so
                    newStartTime = technician.getStartTimeAfternoon();
                }
            } else {
                newStartTime += travelTime;
            }

            //Technician can't start before the end node is able to start
            newStartTime = Math.max(newStartTime, endNode.getStartTime());

            //Save the new start time. insertedStartTime is the start time of the inserted appointment
            if (i >= position && insert) {
                newStartTimes.add(newStartTime);
            } else if (i < position && insert) {
                insertedStartTime = newStartTime;
            }

            // add the appointment duration to find the appointment end time
            newStartTime += endNode.getDurationTime();

            if (newStartTime > endNode.getEndTime() || newStartTime > getTechnician().getEndTimeAfternoon()) // if it ends too late
                return false;
        }

        //Check that the technician has time to go back to the depot
        newStartTime += getTravelTimeToNextAppointment(size() - 1, newStartTime, appointment, position);

        if (newStartTime > getTechnician().getEndTimeAfternoon()) { // if it ends too late
            return false;
        }


        // if no appointment went overtime, update start times and add appointment
        if (insert) {
            //We set the new start times of the appointments following the inserted one
            for (int i = 0; i < newStartTimes.size(); i++) {
                appointmentsStartTime.set(position + i, newStartTimes.get(i));
            }

            if (position <= appointments.size()) {
                appointments.add(position, appointment);
                appointmentsStartTime.add(position, insertedStartTime);
            } else {
                appointments.add(appointment);
                appointmentsStartTime.add(insertedStartTime);
            }
        }

        return true;
    }

    /**
     * This method returns the travel time from an appointment to the next at the given startTime where insertPosition
     * is the position of the node after the one we wish to insert
     * In this method we assume that insertPosition - 1 is the node we wish to insert and insertPosition - 2 is the node
     * before the one we wish to insert
     *
     * @param startPosition  The position from which to leave when getting the travel time
     *                       startPosition must be between 0 and size() - 1
     * @param time           The time at which to get the travel time
     * @param toInsert       The appointment we would like to insert
     * @param insertPosition The insert position of the toInsert appointment
     * @return The travel time between the appointment at the given startNodePosition and its following appointment
     */
    private int getTravelTimeToNextAppointment(int startPosition, int time, Appointment toInsert, int insertPosition) {

        int startId, endId;

        //if we want the travel time of an edge before the inserted appointment
        if (startPosition < insertPosition - 1) {
            startId = (startPosition + 1 < 0 || size() == 0) ? instance.getConstantManager().getDepotId() : get(startPosition + 1).getNodeId();
            if (startPosition + 1 == insertPosition - 1) {
                endId = toInsert.getNodeId();
            } else if (startPosition + 2 < size()) {
                endId = get(startPosition + 2).getNodeId();
            } else {
                endId = instance.getConstantManager().getDepotId();
            }
        }
        //else if we want the travel time from the inserted appointment to the one that would follow it
        else if (startPosition == insertPosition - 1) {
            startId = toInsert.getNodeId();
            endId = (startPosition + 1 >= size()) ? instance.getConstantManager().getDepotId() : get(startPosition + 1).getNodeId();
        }
        //else if we want the travel time between a node to the last one
        else if (startPosition == size() - 1) {
            startId = get(startPosition).getNodeId();
            endId = instance.getConstantManager().getDepotId();
        }
        //Finally we handle the usual case (travel time between appointments already in the route
        else {
            startId = get(startPosition).getNodeId();
            endId = get(startPosition + 1).getNodeId();
        }

        return instance.getFIFOTravellingTime(startId, endId, time);
    }

    /**
     * Removes the node in position <code>i</code> of the route. The removal shifts the nodes to the left
     * (e.g., the node in position <code>i+1<code> moves to <code>i</code>).
     * Also updates the appointment start times
     *
     * @param position the position
     * @return the ID of the removed node
     */
    public Appointment remove(int position) {

        Appointment startNode, endNode;

        int newStartTime = getTechnician().getStartTimeMorning();
        if (position > 0) {
            newStartTime = getAppointmentEndTime(position - 1);
        }

        // remove appointment
        Appointment removed = appointments.remove(position);
        appointmentsStartTime.remove(position);


        // update start times
        for (int i = position; i < size(); i++) {
            endNode = get(i);
            if (i == 0) {
                newStartTime += instance.getEdgeToDepot(get(i)).getTime();
            } else {
                startNode = get(i - 1);
                // add the travel time from previous appointment to this one
                newStartTime += instance.getFIFOTravellingTime(startNode.getNodeId(), endNode.getNodeId(), newStartTime);
            }

            if (newStartTime < endNode.getStartTime()) // if technician arrives before the start time
                newStartTime = endNode.getStartTime(); // wait for the start

            appointmentsStartTime.set(i, newStartTime);

            // add the appointment duration to find the appointment end time
            newStartTime += endNode.getDurationTime();
        }

        return removed;
    }

    /**
     * Calculates the cost of inserting an appointment in a given position. This is a rough, inexact calculation that
     * does not take into account time dependant travel times for subsequent appointments.
     *
     * @param appointment the appointment to insert
     * @param position    the position in the route.
     * @return the cost of inserting the appointment
     */
    public double roughInsertCost(Appointment appointment, int position) {
        double cost = 0;
        int startId, endId;
        int time;
        double distanceDiff = 0;
        int durationDiff = 0;
        EdgeTravelingInformation edge;

        int travelTime;

        if (size() == 0) { // route is empty
            startId = endId = instance.getConstantManager().getDepotId();
            cost += instance.getConstantManager().getTechnician();
            time = technician.getStartTimeMorning();
        } else if (position == 0) { // start node is the depot
            startId = instance.getConstantManager().getDepotId();
            endId = get(position).getNodeId();
            time = technician.getStartTimeMorning();
        } else if (position == size()) { // end node is the depot
            startId = get(position - 1).getNodeId();
            endId = instance.getConstantManager().getDepotId();
            time = getAppointmentEndTime(position - 1);
        } else { // both nodes are regular nodes
            startId = get(position - 1).getNodeId();
            endId = get(position).getNodeId();
            time = getAppointmentEndTime(position - 1);
        }

        if (size() > 0) {
            // remove previous edge
            edge = instance.getTravelingInformation(startId, endId, time);
            travelTime = instance.getFIFOTravellingTime(startId, endId, time);
            durationDiff -= travelTime;
            distanceDiff -= edge.getDistance();
        }

        // add first edge
        edge = instance.getTravelingInformation(startId, appointment.getNodeId(), time);
        travelTime = instance.getFIFOTravellingTime(startId, appointment.getNodeId(), time);
        durationDiff += travelTime;
        distanceDiff += edge.getDistance();

        // add second edge
        // note that here, the time interval is possibly incorrect on purpose
        edge = instance.getTravelingInformation(appointment.getNodeId(), endId, time);
        travelTime = instance.getFIFOTravellingTime(appointment.getNodeId(), endId, time);
        durationDiff += travelTime;
        distanceDiff += edge.getDistance();

        // calculate cost
        cost += distanceDiff * instance.getConstantManager().getKilometre();
        cost += (float) (durationDiff / 60) * instance.getConstantManager().getHourlyFee();

        return cost;
    }

    /**
     * Calculates the cost of removing an appointment in a given position. This is a rough, inexact calculation that
     * does not take into account time dependant travel times for subsequent appointments.
     *
     * @param position the position to remove from the route.
     * @return the cost difference of removing the appointment
     */
    public double roughRemoveCost(int position) {
        double cost = 0;
        int startId, endId;
        int posId = get(position).getNodeId();
        int time;
        double distanceDiff = 0;
        int durationDiff = 0;
        EdgeTravelingInformation edge;

        int travelTime;

        if (size() == 0) { // route is empty
            return 0;
        } else if (size() == 1) { // route only has one appointment
            return -getCost(); // the route is entirely freed
        } else if (position == 0) { // start node is the depot
            startId = instance.getConstantManager().getDepotId();
            endId = get(position + 1).getNodeId();
            time = technician.getStartTimeMorning();
        } else if (position == size() - 1) { // end node is the depot
            startId = get(position - 1).getNodeId();
            endId = instance.getConstantManager().getDepotId();
            time = getAppointmentEndTime(position - 1);
        } else { // both nodes are regular nodes
            startId = get(position - 1).getNodeId();
            endId = get(position + 1).getNodeId();
            time = getAppointmentEndTime(position - 1);
        }

        // remove first edge
        edge = instance.getTravelingInformation(startId, posId, time);
        travelTime = instance.getFIFOTravellingTime(startId, posId, time);
        durationDiff -= travelTime;
        distanceDiff -= edge.getDistance();

        // remove second edge
        edge = instance.getTravelingInformation(posId, endId, time);
        travelTime = instance.getFIFOTravellingTime(posId, endId, time);
        durationDiff -= travelTime;
        distanceDiff -= edge.getDistance();

        // add edge
        edge = instance.getTravelingInformation(startId, endId, time);
        travelTime = instance.getFIFOTravellingTime(startId, endId, time);
        durationDiff += travelTime;
        distanceDiff += edge.getDistance();

        // calculate cost
        cost += distanceDiff * instance.getConstantManager().getKilometre();
        cost += (float) (durationDiff / 60) * instance.getConstantManager().getHourlyFee();

        return cost;
    }

    /**
     * Recalculate the Objective Function of the route. Recalculating it entirely is not optimal (a better way would
     * be to calculate the cost difference after each insertion)
     * Takes into account the technician cost, the technician hourly cost, and kilometer cost.
     */
    public void updateCost() {
        int startId, endId;
        double distance = 0;
        int startTime, endTime;
        int time;
        double duration;

        cost = 0;

        if (size() == 0) // if the technician is not on duty
            return;

        // TECHNICIAN COST
        cost += instance.getConstantManager().getTechnician();

        // TECHNICIAN HOURLY COST
        startId = instance.getConstantManager().getDepotId();
        endId = get(0).getNodeId();
        startTime = getAppointmentStartTime(0)
                - instance.getFIFOTravellingTime(startId, endId, technician.getStartTimeMorning());

        startId = get(size() - 1).getNodeId();
        endId = instance.getConstantManager().getDepotId();
        endTime = getAppointmentEndTime(size() - 1)
                + instance.getFIFOTravellingTime(startId, endId, getAppointmentStartTime(size() - 1));

        duration = (endTime - startTime) / 60;
        cost += duration * instance.getConstantManager().getHourlyFee();

        // KILOMETER COST
        // for each appointment
        for (int i = -1; i < size(); i++) {
            if (i == -1) {
                startId = instance.getConstantManager().getDepotId();
                time = technician.getStartTimeMorning();
            } else {
                startId = get(i).getNodeId();
                time = getAppointmentEndTime(i);
            }
            if (i == size() - 1)
                endId = instance.getConstantManager().getDepotId();
            else
                endId = get(i + 1).getNodeId();

            distance += instance.getTravelingInformation(startId, endId, time).getDistance();
        }

        cost += distance * instance.getConstantManager().getKilometre();
    }

    /**
     * Debug method to check if start times are correct
     *
     * @return true if the start times are correct, false otherwise
     */
    public boolean checkStartTimes() {
        int newStartTime = technician.getStartTimeMorning();
        boolean isMorning = true;
        int startId, endId;
        int travelTime;

        //for each appointment
        for (int i = 0; i < size(); i++) {

            startId = (i == 0) ? instance.getConstantManager().getDepotId() : get(i - 1).getNodeId();
            endId = get(i).getNodeId();
            travelTime = instance.getFIFOTravellingTime(startId, endId, newStartTime);

            int projectedEndTime = Math.max(newStartTime + travelTime, get(i).getStartTime()) + get(i).getDurationTime();

            if (isMorning && projectedEndTime > technician.getEndTimeMorning()) {
                isMorning = false;

                //If it isn't possible to travel to the next appointment before the end of the morning, do it first thing in the afternoon
                if (newStartTime + travelTime >= technician.getEndTimeMorning()) {
                    travelTime = instance.getFIFOTravellingTime(startId, endId, newStartTime);
                    newStartTime = technician.getStartTimeAfternoon() + travelTime;
                } else {//if it is possible to travel to the next appointment before the end of the morning, do so
                    newStartTime = technician.getStartTimeAfternoon();
                }
            } else {
                newStartTime += travelTime;
            }

            newStartTime = Math.max(newStartTime, get(i).getStartTime());

            if (newStartTime != getAppointmentStartTime(i))
                return false;

            // add the appointment duration to find the appointment end time
            newStartTime += get(i).getDurationTime();
        }

        Appointment appointment = get(size() - 1);
        int startTime = appointmentsStartTime.get(size() - 1);
        if (startTime + appointment.getDurationTime() + instance.getEdgeToDepot(get(size() - 1)).getTime() > technician.getEndTimeAfternoon()) {
            return false;
        }

        return true;
    }

    public void setInstance(InstanceDistanceMatrix instance) {
        this.instance = instance;
    }

    @Override
    public AdvancedRoute clone() {

        AdvancedRoute clone = new AdvancedRoute(instance);
        for (Appointment a : appointments) {
            clone.add(a);
        }
        for (int i = 0; i < size(); i++) {
            clone.addAppointmentStartTime(appointmentsStartTime.get(i));
        }
        clone.setCost(cost);
        clone.setTechnician(technician);

        return clone;
    }
}
