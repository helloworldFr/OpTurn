package fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.appointmentselection;

import fr.univ_tours.polytech.di4.project.algorithm.solution.AdvancedRoute;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;
import fr.univ_tours.polytech.di4.project.data.instance.InstanceDistanceMatrix;

/**
 * Created by Theo on 28/04/2016.
 * This class is a type of AppointmentSelectionHeuristic that tries to maximize the benefit derived from going to an
 * appointment on the current route rather than on a new route (see Solomon, 1987)
 */
public class RouteKeepingASHeuristic extends AppointmentSelectionHeuristic {

    /* The instance to know the cost of starting a new route */
    protected InstanceDistanceMatrix instance;

    /* The route creation factor that must be greater or equal to 0 */
    protected double routeCreationFactor = 1;

    /**
     * RouteKeepingASHeuristic constructor
     *
     * @param instance The instance to use
     */
    public RouteKeepingASHeuristic(InstanceDistanceMatrix instance) {
        this.instance = instance;
    }

    /**
     * Sets the route creation factor
     * If the route creation factor is less than 0, it will be set to 0
     *
     * @param routeCreationFactor The route creation factor to set
     */
    public void setRouteCreationFactor(double routeCreationFactor) {
        this.routeCreationFactor = (routeCreationFactor < 0) ? 0 : routeCreationFactor;
    }

    /**
     * Calculates the cost of going to the given appointment using a newly created route minus the cost of adding
     * the appointment to the current route
     *
     * @param r         The route in which the appointment would be inserted
     * @param insertPos The position at which the appointment would be inserted
     * @param toInsert  The appointment that would be inserted
     * @return The calculated cost
     */
    public double appointmentSelectionCost(AdvancedRoute r, int insertPos, Appointment toInsert) {

        double distance = instance.getTravelingInformation(instance.getConstantManager().getDepotId(),
                toInsert.getNodeId(),
                0).getDistance();

        return instance.getConstantManager().getTechnician()
                + routeCreationFactor * distance
                - insertionPositionHeuristic.getInsertionCost(r, insertPos, toInsert);
    }

    /**
     * Compare two costs
     *
     * @param cost1 The first cost to compare
     * @param cost2 The second cost to compare
     * @return -1 if cost1 is better than cost2, 0 if they are equivalent, 1 otherwise
     */
    @Override
    public int compare(double cost1, double cost2) {
        if (cost1 < cost2)
            return 1;
        if (cost1 == cost2)
            return 0;

        return -1;
    }
}
