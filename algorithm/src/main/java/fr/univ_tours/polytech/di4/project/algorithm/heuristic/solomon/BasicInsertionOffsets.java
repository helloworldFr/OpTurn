package fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon;

import fr.univ_tours.polytech.di4.project.algorithm.solution.AdvancedRoute;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;
import fr.univ_tours.polytech.di4.project.data.instance.EdgeTravelingInformation;
import fr.univ_tours.polytech.di4.project.data.instance.InstanceDistanceMatrix;

/**
 * Created by Theo on 04/05/2016.
 * Class used by the heuristics to calculate the offsets of time and distance that appear when inserting an appointment
 * in a route
 */
public class BasicInsertionOffsets {

    /**
     * Internal class that is used to return various calculated offsets needed to calculate the cost
     */
    public class BasicOffsets {
        private double timeOffset, distanceOffset;

        public BasicOffsets(double timeOffset, double distanceOffset) {
            this.timeOffset = timeOffset;
            this.distanceOffset = distanceOffset;
        }

        public double getTimeOffset() {
            return timeOffset;
        }

        public double getDistanceOffset() {
            return distanceOffset;
        }
    }

    /**
     * distance matrix used to calculate the insertion cost
     */
    private InstanceDistanceMatrix instance;

    /**
     * Constructor of the BasicInsertionOffsets class
     * Set the instance to use to calculate cost offsets
     *
     * @param instance The instance to use
     */
    public BasicInsertionOffsets(InstanceDistanceMatrix instance) {
        this.instance = instance;
    }

    /**
     * This method calculates the approximate offset in cost when inserting an appointment in a route at a given position
     *
     * @param route     The route in which to insert the appointment
     * @param insertPos The position at which to insert the appointment
     * @param toInsert  The appointment that would be inserted
     * @return A BasicOffsets instance containing the time and distance offset
     */
    public BasicOffsets calculateBasicOffsets(AdvancedRoute route, int insertPos, Appointment toInsert) {

        Appointment previousAppointment = null, nextAppointment = null;

        if (insertPos > 0 && insertPos < route.size()) {
            previousAppointment = route.get(insertPos - 1);
        }
        //If the appointment is to be inserted at the end of the route then there should be no 'next' appointment
        if (insertPos < route.size() && insertPos >= 0) {
            nextAppointment = route.get(insertPos);
        }

        double distOffset = 0;
        double timeOffset = 0;

        int travelTime;

        //If there will be an appointment before the one we insert
        if (previousAppointment != null) {
            //Contains data corresponding to the edge that would precede the inserted appointment */
            EdgeTravelingInformation futureFrontEI = instance.getTravelingInformation(previousAppointment.getNodeId(),
                    toInsert.getNodeId(),
                    route.getAppointmentEndTime(insertPos - 1));

            travelTime = instance.getFIFOTravellingTime(previousAppointment.getNodeId(),
                    toInsert.getNodeId(),
                    route.getAppointmentEndTime(insertPos - 1));

            distOffset += futureFrontEI.getDistance();
            timeOffset += travelTime + toInsert.getDurationTime();
        } else {
            EdgeTravelingInformation edgeToDepot = instance.getEdgeToDepot(toInsert);
            distOffset += edgeToDepot.getDistance();
            timeOffset += edgeToDepot.getTime();

            if (nextAppointment != null) {
                edgeToDepot = instance.getEdgeToDepot(nextAppointment);
                distOffset -= edgeToDepot.getDistance();
                timeOffset -= edgeToDepot.getTime();
            }
        }

        //If there will be an appointment after the one we insert
        if (nextAppointment != null) {
            //Contains data corresponding to the edge that would be after the inserted appointment */
            EdgeTravelingInformation futureBackEI = instance.getTravelingInformation(toInsert.getNodeId(),
                    nextAppointment.getNodeId(),
                    getMinimumInsertionTime(route, insertPos, toInsert));

            travelTime = instance.getFIFOTravellingTime(toInsert.getNodeId(),
                    nextAppointment.getNodeId(),
                    getMinimumInsertionTime(route, insertPos, toInsert));

            distOffset += futureBackEI.getDistance();
            timeOffset += travelTime;

        } else {
            EdgeTravelingInformation edgeToDepot = instance.getEdgeToDepot(toInsert);
            distOffset += edgeToDepot.getDistance();
            timeOffset += edgeToDepot.getTime();

            if (previousAppointment != null) {
                edgeToDepot = instance.getEdgeToDepot(previousAppointment);
                distOffset -= edgeToDepot.getDistance();
                timeOffset -= edgeToDepot.getTime();
            }
        }

        // Finally we calculate the offset when removing the route that has to be destroyed if there is one
        if (previousAppointment != null && nextAppointment != null) {

            // Contains data corresponding to the edge that exists at the moment */
            EdgeTravelingInformation currentEI = instance.getTravelingInformation(previousAppointment.getNodeId(),
                    nextAppointment.getNodeId(),
                    route.getAppointmentEndTime(insertPos - 1));

            travelTime = instance.getFIFOTravellingTime(previousAppointment.getNodeId(),
                    nextAppointment.getNodeId(),
                    route.getAppointmentEndTime(insertPos - 1));

            distOffset -= currentEI.getDistance();
            timeOffset -= travelTime;
        }

        return new BasicOffsets(timeOffset, distOffset);
    }

    /**
     * Returns the minimum time at which an appointment can start when it is at a given position in a given route
     *
     * @param r         The route in which the appointment would be inserted
     * @param insertPos The position at which the appointment would be inserted
     * @param toInsert  The appointment that would be inserted in the route
     * @return The minimum time at which the appointment can start
     */
    public int getMinimumInsertionTime(AdvancedRoute r, int insertPos, Appointment toInsert) {

        int minTime = toInsert.getStartTime();
        int minInsert = 0;

        if (insertPos > 0) {
            minInsert = r.getAppointmentEndTime(insertPos - 1);
        } else {
            minInsert = r.getTechnician().getStartTimeMorning();
        }

        return Math.max(minTime, minInsert);
    }
}
