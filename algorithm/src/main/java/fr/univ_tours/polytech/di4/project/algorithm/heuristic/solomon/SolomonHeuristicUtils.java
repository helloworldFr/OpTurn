package fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon;

import fr.univ_tours.polytech.di4.project.algorithm.solution.AdvancedRoute;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Gilbert on 21/04/2016.
 * This class offers a set of methods used to select which appointment should be added to which
 */
public class SolomonHeuristicUtils {

    /* Object used to get the best insert position and the best appointment to insert */
    private SolomonCosts solomonCosts;

    /**
     * Constructor for the SolomonHeuristicUtils class
     *
     * @param solomonCosts The SolomonCosts instance
     */
    public SolomonHeuristicUtils(SolomonCosts solomonCosts) {
        this.solomonCosts = solomonCosts;
    }

    /**
     * Getter of solomonCosts
     *
     * @return the solomonCost associated with this instance
     */
    public SolomonCosts getSolomonCosts() {
        return solomonCosts;
    }

    /**
     * This method returns for each given appointment the position where it would be best to insert them on the given
     * route
     * To do so it uses the insertPositionSelectionCost() method from the SolomonCosts class
     *
     * @param r            The route in which an appointment would be inserted
     * @param appointments The appointments of which we want to calculate their best insert position
     * @return A map containing for each given appointment a feasible insert position that would add the lowest cost to
     * the route
     */
    public Map<Appointment, Integer> getBestInsertPositions(AdvancedRoute r, List<Appointment> appointments) {

        Map<Appointment, Integer> insertPositions = new HashMap<Appointment, Integer>();

        for (Appointment u : appointments) {

            double cost, bestCost = Double.MAX_VALUE;

            for (int insertPos = 0; insertPos <= r.size(); insertPos++) {
                cost = solomonCosts.insertPositionSelectionCost(r, insertPos, u);

                //if cost is an improvement over bestCost and insertion is possible
                if (solomonCosts.getCurrentInsertionPositionHeuristic().compare(bestCost, cost) > 0) {
                    if (r.isInsertionPossible(u, insertPos)) {
                        bestCost = cost;
                        insertPositions.put(u, insertPos);
                    }
                }
            }
        }

        return insertPositions;
    }

    /**
     * This method returns for a set of given appointments and their respective best insert position, the appointment
     * that would be best to add to the given route
     * This method adds in no way this appointment to the given route
     * To do so it uses the appointmentSelectionCost() method from the SolomonCosts class
     *
     * @param r            The route in which an appointment would be inserted
     * @param appointments The appointments from which to select the best appointment to insert
     * @return The best appointment to insert
     */
    public Appointment getBestAppointmentToInsert(AdvancedRoute r, Map<Appointment, Integer> appointments) {

        Appointment bestAppointment = null;
        double cost;
        double bestCost;

        //if optimum is min, the initial value of bestCost is MAX_VALUE, otherwise it is MIN_VALUE
        if (solomonCosts.getCurrentAppointmentSelectionHeuristic().compare(1, 0) > 0) {
            bestCost = Double.MAX_VALUE;
        } else {
            bestCost = Double.MIN_VALUE;
        }

        for (Appointment a : appointments.keySet()) {

            cost = solomonCosts.appointmentSelectionCost(r, appointments.get(a), a);
            //if cost is an improvement over bestCost
            if (solomonCosts.getCurrentAppointmentSelectionHeuristic().compare(bestCost, cost) > 0) {
                bestCost = cost;
                bestAppointment = a;
            }
        }

        return bestAppointment;
    }

}
