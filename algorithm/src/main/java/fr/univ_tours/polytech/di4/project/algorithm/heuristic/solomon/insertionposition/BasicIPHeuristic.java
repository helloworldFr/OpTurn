package fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.insertionposition;

import fr.univ_tours.polytech.di4.project.algorithm.heuristic.solomon.BasicInsertionOffsets;
import fr.univ_tours.polytech.di4.project.algorithm.solution.AdvancedRoute;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;
import fr.univ_tours.polytech.di4.project.data.instance.InstanceDistanceMatrix;

/**
 * Created by Theo on 27/04/2016.
 * Basic heuristic that is used most of the time to decide where to insert an appointment in a route
 */
public class BasicIPHeuristic extends InsertionPositionHeuristic {

    /* The BasicInsertionOffsets instance to use to calculate offset costs on adding an appointment to a route */
    protected BasicInsertionOffsets basicInsertionOffsets;

    /**
     * Constructor of the BasicIPHeuristic class
     *
     * @param distanceMatrix The distance matrix
     */
    public BasicIPHeuristic(InstanceDistanceMatrix distanceMatrix) {
        basicInsertionOffsets = new BasicInsertionOffsets(distanceMatrix);
    }

    /**
     * Implementation of the getInsertionCost method
     * This implementation takes into account the delta-time and the delta-distance of the insertion
     *
     * @param route     The route in which the appointment would be inserted
     * @param insertPos The position at which the appointment would be inserted
     * @param toInsert  The appointment that would be inserted
     * @return The cost of inserting the given appointment in the given route at the given position
     */
    public double getInsertionCost(AdvancedRoute route, int insertPos, Appointment toInsert) {

        BasicInsertionOffsets.BasicOffsets basicOffsets = basicInsertionOffsets.calculateBasicOffsets(route, insertPos, toInsert);

        return timeFactor * basicOffsets.getTimeOffset() + distanceFactor * basicOffsets.getDistanceOffset();
    }
}
