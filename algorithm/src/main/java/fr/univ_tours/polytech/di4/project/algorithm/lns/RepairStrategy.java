package fr.univ_tours.polytech.di4.project.algorithm.lns;

import fr.univ_tours.polytech.di4.project.algorithm.solution.Solution;
import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;

import java.util.List;

/**
 * Strategy pattern for the repair heuristics for the Large Neighborhood Search
 * Possible strategies:
 * - regret-q
 * - place nodes in the longest route first
 */
public interface RepairStrategy {

    /**
     * Repairs (insert missing nodes into) a solution after it was destroyed
     *
     * @param solution      the solution to repair
     * @param removed       the list of removed appointments, resulting from a call to a destroy heuristic
     * @param startPosition the position of the first node to be inserted back in the solution
     */
    Solution repair(Solution solution, List<Appointment> removed, int startPosition);
}
