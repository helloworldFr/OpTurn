package fr.univ_tours.polytech.di4.project.algorithm.solution;

import fr.univ_tours.polytech.di4.project.data.appointment.Appointment;
import fr.univ_tours.polytech.di4.project.data.technician.Technician;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Théo on 01/04/2016.
 */
public class Route implements Cloneable, Serializable {

    protected List<Appointment> appointments = new ArrayList<Appointment>();

    protected List<Integer> appointmentsStartTime = new ArrayList<Integer>();

    protected double cost = 0;

    protected Technician technician;


    /**
     * @return the number of nodes in the route
     */
    public int size() {
        return appointments.size();
    }

    /**
     * @param appointment the appointment
     * @return true if the route contains node <code>appointment</code>, false otherwise
     */
    public boolean contains(Appointment appointment) {
        return appointments.contains(appointment);
    }

    /**
     * @param appointment
     * @return Returns the position of the first occurrence of node <code>appointment</code> in the route,
     * or -1 if the route does not contain the node.
     */
    public int positionOf(Appointment appointment) {
        return appointments.indexOf(appointment);
    }

    /**
     * @param i
     * @return the appointment in position <code>i</code>
     */
    public Appointment get(int i) {
        return appointments.get(i);
    }

    /**
     * Appends node <code>appointment</code> to the route (i.e., at the last position of the route).
     * Does NOT add an appointment start time
     *
     * @param appointment the node to append
     */
    public void add(Appointment appointment) {
        appointments.add(appointment);
    }

    /**
     * @return the list of customers in the route. Nodes that appear in the list are in the same order
     * in which they are visited by the route.
     */
    public List<Appointment> getRoute() {
        return appointments;
    }


    /**
     * Get the cost of the route
     *
     * @return double
     */
    public double getCost() {
        return cost;
    }

    /**
     * Set the cost of the route
     *
     * @param cost the cost
     */
    public void setCost(double cost) {
        this.cost = cost;
    }

    /**
     * Sets the technician associated with the route
     *
     * @param technician
     */
    public void setTechnician(Technician technician) {
        this.technician = technician;
    }

    /**
     * Returns the technician associated with the route
     *
     * @return Technician
     */
    public Technician getTechnician() {
        return technician;
    }

    /**
     * Sets the start time of the given appointment
     *
     * @param i    The appointment to set the time to
     * @param time The start time to set to the appointment
     */
    public void setAppointmentStartTime(int i, int time) {
        appointmentsStartTime.set(i, time);
    }

    /**
     * Adds an appointment start time
     *
     * @param time The start time to set to the appointment
     */
    public void addAppointmentStartTime(int time) {
        appointmentsStartTime.add(time);
    }

    /**
     * Returns the start time of the i'th appointment of the route
     *
     * @param i The index of the appointment in the route
     * @return The start time of the i'th appointment in minutes
     */
    public int getAppointmentStartTime(int i) {
        return appointmentsStartTime.get(i);
    }

    /**
     * Returns the end time of the i'th appointment of the route
     *
     * @param i The index of the appointment in the route
     * @return The start time of the i'th appointment in minutes
     */
    public int getAppointmentEndTime(int i) {
        return getAppointmentStartTime(i) + get(i).getDurationTime();
    }

    /**
     * @return a hard copy of the route
     */
    public Route clone() {
        Route clone = new Route();
        for (Appointment a : appointments) {
            clone.add(a);
        }
        for (int i = 0; i < size(); i++) {
            clone.setAppointmentStartTime(i, appointmentsStartTime.get(i));
        }
        clone.setCost(cost);
        clone.setTechnician(technician);

        return clone;
    }

    /**
     * Compares the route with a given object. Returns true if both are routes composed of the same appointments
     * in the same order
     * Equals is needed to be reimplemented because we need to be able to check that we do not have twice the same
     * route to check that we do not have twice the same solution
     *
     * @param o The object to compare the route to
     * @return True if the object is a route that contains appointments equal to the current one and in the same order
     * False otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (!o.getClass().equals(getClass()))
            return false;

        Route route = (Route) o;
        List<Appointment> solutionAppointments = route.getRoute();

        //If the solutions don't have the same amount of routes, then they are not equal
        if (solutionAppointments.size() != appointments.size())
            return false;

        //If any of the appointments is not contained in the given route, then the routes are not equal
        for (int i = 0; i < appointments.size(); i++) {
            if (!appointments.get(i).equals(solutionAppointments.get(i)))
                return false;
        }
        //If any of the start time is different, then the routes are not equal
        for (int i = 0; i < appointmentsStartTime.size(); i++) {
            if (!appointments.get(i).equals(route.getAppointmentStartTime(i)))
                return false;
        }

        return true;
    }

    /**
     * The hashCode method should be consistent with the equals method (two objects that are equal should return the
     * same hashcode)
     * For this reason it needs to be reimplemented when implementing a route
     *
     * @return The hashcode of the given route
     */
    @Override
    public int hashCode() {
        int hashCode = (int) cost;

        for (Appointment a : appointments) {
            hashCode += a.hashCode();
        }

        for (Integer i : appointmentsStartTime) {
            hashCode += i;
        }

        return hashCode;
    }
}