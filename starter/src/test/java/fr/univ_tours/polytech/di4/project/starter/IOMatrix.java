package fr.univ_tours.polytech.di4.project.starter;

import fr.univ_tours.polytech.di4.project.data.instance.InstanceDistanceMatrix;
import fr.univ_tours.polytech.di4.project.io.IOInstanceDistanceMatrix;

/**
 * Created by Timothy Nibeaudeau on 20/04/2016 for BaseProject.
 */
public class IOMatrix {
    static public void main(String[] args) {
        IOInstanceDistanceMatrix instanceIO = new IOInstanceDistanceMatrix(LoadProperties.getProperties());
        final InstanceDistanceMatrix instance = instanceIO.parserReadFromSource("test.rl");
    }
}
