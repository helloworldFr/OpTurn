package fr.univ_tours.polytech.di4.project.starter;

import fr.univ_tours.polytech.di4.project.algorithm.Algorithm;
import fr.univ_tours.polytech.di4.project.algorithm.SequentialAlgorithm;
import fr.univ_tours.polytech.di4.project.algorithm.lns.LargeNeighborhoodSearch;
import fr.univ_tours.polytech.di4.project.algorithm.solution.AdvancedRoute;
import fr.univ_tours.polytech.di4.project.algorithm.solution.Solution;
import fr.univ_tours.polytech.di4.project.checker.Checker;
import fr.univ_tours.polytech.di4.project.data.instance.InstanceDistanceMatrix;
import fr.univ_tours.polytech.di4.project.io.IOInstanceDistanceMatrix;
import fr.univ_tours.polytech.di4.project.io.solution.SolutionWriter;

import java.util.Properties;

/**
 * Created by Timothy Nibeaudeau on 03/06/2016 for BaseProject.
 */
public class GoogleTestOpTurn {
    private static InstanceDistanceMatrix instanceDistanceMatrix = null;


    public static void main(String[] args) {
        Properties properties = LoadProperties.getProperties();
        if (!properties.isEmpty()) {
            String filename = args[0];
            if (args.length <= 0 || (!filename.contains(".rl") && !filename.contains(".json"))) {
                throw new IllegalArgumentException("You need to give source file (.rl) or the matrix file (.json)");
            }

            if (filename.contains(".rl")) {
                //create an instance
                GetResources.main(args);
                instanceDistanceMatrix = GetResources.getInstanceDistanceMatrix();
            } else {
                instanceDistanceMatrix = IOInstanceDistanceMatrix.readJson(filename);
            }

            Algorithm algorithm = new SequentialAlgorithm(instanceDistanceMatrix);
            Solution solution = algorithm.run(properties);

            Checker checker = new Checker(solution.getRoutes().toArray(new AdvancedRoute[solution.getRoutes().size()]), instanceDistanceMatrix);
            System.out.println("Solution OF : " + solution.getOF() + " with " + solution.countFilledRoute() + " routes " + solution.countAllAppointment() + " appointments / " + instanceDistanceMatrix.getAppointmentManager().getAppointments().size());
            if (checker.checkSolution()) {
                System.out.println("Success ! :)");
            } else {
                System.err.println("Failure ! :(");
            }

            Solution backup = solution.clone();

            System.out.println("Running LNS");
            LargeNeighborhoodSearch lns = new LargeNeighborhoodSearch();
            boolean success = true;
            solution = lns.run(properties, solution);

            checker = new Checker(solution.getRoutes().toArray(new AdvancedRoute[solution.getRoutes().size()]), instanceDistanceMatrix);
            if (checker.checkSolution()) {
                System.out.println("Success ! :)");
            } else {
                System.err.println("Failure ! :( Retry once");
                solution = lns.run(properties, backup);
                checker = new Checker(solution.getRoutes().toArray(new AdvancedRoute[solution.getRoutes().size()]), instanceDistanceMatrix);
                if (checker.checkSolution()) {
                    System.out.println("Success ! :)");
                } else {
                    System.err.println("Failure ! :( again");
                    success = false;
                }
            }

            if (success) {
                System.out.println("LNS -> Solution OF : " + solution.getOF() + " with " + solution.countFilledRoute() + " routes " + solution.countAllAppointment() + " appointments / " + instanceDistanceMatrix.getAppointmentManager().getAppointments().size());
            }

            SolutionWriter solutionWriter = new SolutionWriter();
            solutionWriter.writeSolution(solution, filename + "_solution.json");

        }
    }

}