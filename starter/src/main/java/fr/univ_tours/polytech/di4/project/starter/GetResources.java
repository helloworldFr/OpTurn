package fr.univ_tours.polytech.di4.project.starter;

import fr.univ_tours.polytech.di4.project.data.geographical.GeographicalNode;
import fr.univ_tours.polytech.di4.project.data.instance.EdgeTravelingInformation;
import fr.univ_tours.polytech.di4.project.data.instance.InstanceDistanceMatrix;
import fr.univ_tours.polytech.di4.project.data.time_interval.TimeInterval;
import fr.univ_tours.polytech.di4.project.io.IOInstanceDistanceMatrix;
import fr.univ_tours.polytech.di4.project.io.edge_builder.EdgeTravelingBuilder;
import fr.univ_tours.polytech.di4.project.io.edge_builder.EdgeTravelingBuilderGoogleAPI;
import org.joda.time.DateTime;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.concurrent.*;

/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This class is use to create the matrix which contain all duration time from Google APi and save the result in an output file
 */
public class GetResources {
    private static InstanceDistanceMatrix instanceDistanceMatrix = null;

    /**
     * This main handle the creation of the instance and can recovery from a previous backup file
     *
     * @param args first param .rl file, second param (optional) filename of the backup file
     */
    public static void main(String[] args) {
        Properties properties = LoadProperties.getProperties();
        try {
            if (!properties.isEmpty()) {
                String filename = args[0];
                if (args.length <= 0 || (!filename.contains(".rl"))) {
                    throw new IllegalArgumentException("You need to give source file (.rl) or the matrix file (.json)");
                }
                if (filename.contains(".rl")) {
                    //create an instance
                    if (args.length > 1) {
                        buildMatrix(filename, args[1], properties);
                    } else {
                        buildMatrix(filename, null, properties);
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Build matrix from source rl (request on google map) by multithreading
     *
     * @param filename   of the source
     * @param properties of the app
     * @throws InterruptedException if something wrong with the app
     */
    private static void buildMatrix(String filename, String backupFilename, Properties properties) throws InterruptedException {
        IOInstanceDistanceMatrix ioInstanceDistanceMatrix = new IOInstanceDistanceMatrix(properties);
        instanceDistanceMatrix = ioInstanceDistanceMatrix.parserReadFromSource(filename);

        final EdgeTravelingBuilder travelingBuilder = new EdgeTravelingBuilderGoogleAPI(properties.getProperty("GOOGLE_MAP_API_KEY"), backupFilename);
        final DateTime midnight = getMidnightTuesday();

        long startTime = System.currentTimeMillis();
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        CompletionService<ResultEdge> completionService = new ExecutorCompletionService<ResultEdge>(executorService);

        for (final GeographicalNode nodeSource : instanceDistanceMatrix.getNodes()) {
            for (final GeographicalNode nodeDestination : instanceDistanceMatrix.getNodes()) {
                for (final TimeInterval interval : instanceDistanceMatrix.getTimeIntervals()) {
                    completionService.submit(new Callable<ResultEdge>() {
                        public ResultEdge call() throws Exception {
                            DateTime time = midnight.plusMinutes(interval.getStartTime());// first our of the interval
                            EdgeTravelingInformation edge = travelingBuilder.createEdge(nodeSource, nodeDestination, time);

                            ResultEdge result = new ResultEdge();
                            result.edgeTravelingInformation = edge;
                            result.sourceId = nodeSource.getId();
                            result.destinationId = nodeDestination.getId();
                            result.intervalId = interval.getId();

                            return result;
                        }
                    });
                }
            }
        }

        for (final GeographicalNode nodeSource : instanceDistanceMatrix.getNodes()) {
            for (final GeographicalNode nodeDestination : instanceDistanceMatrix.getNodes()) {
                for (final TimeInterval interval : instanceDistanceMatrix.getTimeIntervals()) {
                    try {
                        ResultEdge resultEdge = completionService.take().get();
                        instanceDistanceMatrix.setTravelingInformation(resultEdge.edgeTravelingInformation, resultEdge.sourceId, resultEdge.destinationId, resultEdge.intervalId);
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        executorService.shutdown();
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            throw e;
        }
        travelingBuilder.closeBackup();
        System.out.println(System.currentTimeMillis() - startTime + " ms with " + EdgeTravelingBuilderGoogleAPI.getNumberRequest() + " request");
        //save it
        String outFilename = filename.split("\\.")[0] + ".json";
        ioInstanceDistanceMatrix.writeJson(outFilename);
    }

    static class ResultEdge {
        EdgeTravelingInformation edgeTravelingInformation;
        int sourceId;
        int destinationId;
        int intervalId;
    }

    /**
     * This method is use to get a DateTime to request Google API
     *
     * @return dateTime next tuesday at 00:00
     */
    private static DateTime getMidnightTuesday() {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_WEEK, 1);//jump to tomorrow
        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.TUESDAY) {
            calendar.add(Calendar.DAY_OF_WEEK, 1);
        }
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return new DateTime(calendar.getTime());
    }

    public static InstanceDistanceMatrix getInstanceDistanceMatrix() {
        return instanceDistanceMatrix;
    }
}
