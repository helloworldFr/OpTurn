package fr.univ_tours.polytech.di4.project.starter;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by Timothy Nibeaudeau on 20/04/2016 for BaseProject.
 */
abstract class LoadProperties {
    /**
     * Get the main config from config.xml
     *
     * @return config Properties
     */
    static Properties getProperties() {
        Properties config = new Properties();
        try {
            config.loadFromXML(new FileInputStream("config.xml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return config;
    }
}
