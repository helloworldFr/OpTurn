package fr.univ_tours.polytech.di4.project.starter;

import fr.univ_tours.polytech.di4.project.data.geographical.GeographicalNode;
import fr.univ_tours.polytech.di4.project.data.instance.EdgeTravelingInformation;
import fr.univ_tours.polytech.di4.project.data.instance.InstanceDistanceMatrix;
import fr.univ_tours.polytech.di4.project.data.instance.InstanceDistanceMatrixMetric;
import fr.univ_tours.polytech.di4.project.io.IOInstanceDistanceMatrix;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This class is use to write the metric from JSON matrix to give some ideas of the quality of the matrix
 */
public class GetMetrics {
    static public void main(String[] args) {
        if (args.length <= 0) {
            throw new IllegalArgumentException("You need to give the matrix file (.json)");
        }
        String filename = args[0];
        if (!filename.contains(".json")) {
            throw new IllegalArgumentException("You need to give the matrix file (.json)");
        }
        InstanceDistanceMatrix instanceDistanceMatrix = IOInstanceDistanceMatrix.readJson(filename);

        InstanceDistanceMatrixMetric[][] metrics = buildMetrics(instanceDistanceMatrix);
        String outFilename = filename.split("\\.")[0];
        IOInstanceDistanceMatrix.writeInstanceDistanceMatrixMetrics(metrics, outFilename);
    }


    private static InstanceDistanceMatrixMetric[][] buildMetrics(InstanceDistanceMatrix instance) {
        List<GeographicalNode> nodes = instance.getNodes();
        InstanceDistanceMatrixMetric[][] metrics = new InstanceDistanceMatrixMetric[nodes.size()][nodes.size()];

        for (int i = 0; i < nodes.size(); i++) {
            GeographicalNode nodeSource = nodes.get(i);

            for (int j = 0; j < nodes.size(); j++) {
                GeographicalNode nodeDestination = nodes.get(j);
                InstanceDistanceMatrixMetric metric = new InstanceDistanceMatrixMetric();
                metric.setOccurrences(new HashMap<Integer, Integer>());

                List<EdgeTravelingInformation> edgeTravelingInformations = Arrays.asList(instance.getAllTimeIntervalTravelingInformationMatrix(nodeSource.getId(), nodeDestination.getId()));

                EdgeTravelingInformation min = Collections.min(edgeTravelingInformations);
                EdgeTravelingInformation max = Collections.max(edgeTravelingInformations);

                for (EdgeTravelingInformation edgeTravelingInformation : edgeTravelingInformations) {
                    int time = edgeTravelingInformation.getTime();
                    if (!metric.getOccurrences().containsKey(time)) {
                        metric.getOccurrences().put(time, 1);//Collections.frequency(edgeTravelingInformations, edgeTravelingInformation));
                    } else {
                        int number = metric.getOccurrences().get(time);
                        metric.getOccurrences().put(time, number + 1);
                    }
                }

                metric.setMin(min.getTime());
                metric.setMax(max.getTime());

                metrics[i][j] = metric;
            }
        }
        return metrics;

    }
}
