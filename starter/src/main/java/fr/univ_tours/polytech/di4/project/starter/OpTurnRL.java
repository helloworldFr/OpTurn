package fr.univ_tours.polytech.di4.project.starter;

import fr.univ_tours.polytech.di4.project.algorithm.Algorithm;
import fr.univ_tours.polytech.di4.project.algorithm.SequentialAlgorithm;
import fr.univ_tours.polytech.di4.project.algorithm.lns.LargeNeighborhoodSearch;
import fr.univ_tours.polytech.di4.project.algorithm.solution.AdvancedRoute;
import fr.univ_tours.polytech.di4.project.algorithm.solution.Solution;
import fr.univ_tours.polytech.di4.project.checker.Checker;
import fr.univ_tours.polytech.di4.project.data.geographical.GeographicalNode;
import fr.univ_tours.polytech.di4.project.data.instance.EdgeTravelingInformation;
import fr.univ_tours.polytech.di4.project.data.instance.InstanceDistanceMatrix;
import fr.univ_tours.polytech.di4.project.data.time_interval.TimeInterval;
import fr.univ_tours.polytech.di4.project.io.IOInstanceDistanceMatrix;
import fr.univ_tours.polytech.di4.project.io.edge_builder.EdgeTravelingBuilder;
import fr.univ_tours.polytech.di4.project.io.edge_builder.EdgeTravelingBuilderFromRL;
import fr.univ_tours.polytech.di4.project.io.solution.SolutionWriter;
import org.joda.time.DateTime;

import java.io.File;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Mathieu on 18/03/2016.
 */
public class OpTurnRL {

    static private InstanceDistanceMatrix loadInstance(String filename) {

        Properties config = LoadProperties.getProperties();

        IOInstanceDistanceMatrix instanceIO = new IOInstanceDistanceMatrix(config);
        final InstanceDistanceMatrix instance = instanceIO.parserReadFromSource(filename + ".rl");

        //ONLY need when using EdgeTravelingBuilderFromRL
        int rowSize = instanceIO.getDistances().length;

        if (rowSize > 0) {

            int colSize = instanceIO.getDistances()[0].length;
            double[][] distances = new double[rowSize][colSize];
            int[][] times = new int[rowSize][colSize];

            for (int i = 0; i < rowSize; i++) {
                for (int j = 0; j < colSize; j++) {
                    distances[i][j] = Double.parseDouble(instanceIO.getDistances()[i][j]);
                    times[i][j] = (int) Double.parseDouble(instanceIO.getTimes()[i][j]);
                }
            }

            final EdgeTravelingBuilder travelingBuilder = new EdgeTravelingBuilderFromRL(distances, times, instance.getAppointmentManager());//new EdgeTravelingBuilderGoogleAPI(config.getProperty("GOOGLE_MAP_API_KEY"), null);
            final DateTime midnight = getMidnightTuesday();

            ExecutorService executorService = Executors.newFixedThreadPool(10);

            for (final GeographicalNode nodeSource : instance.getNodes()) {
                for (final GeographicalNode nodeDestination : instance.getNodes()) {
                    for (final TimeInterval interval : instance.getTimeIntervals()) {
                        executorService.submit(new Runnable() {
                            public void run() {
                                EdgeTravelingInformation edge = travelingBuilder.createEdge(nodeSource, nodeDestination, midnight.plusMinutes(interval.getStartTime()));

                                instance.setTravelingInformation(edge, nodeSource.getId(), nodeDestination.getId(), interval.getId());
                            }
                        });
                    }
                }
            }
            executorService.shutdown();
            try {
                executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            travelingBuilder.closeBackup();

            instanceIO.writeJson(filename + "_instance_testRL.json");
        }

        return instance;
    }

    static private Solution runAlgorithm(InstanceDistanceMatrix instance, String filename) {
        Properties properties = LoadProperties.getProperties();
        Algorithm algorithm = new SequentialAlgorithm(instance);
        Solution solution = algorithm.run(properties);

        Checker checker = new Checker(solution.getRoutes().toArray(new AdvancedRoute[solution.getRoutes().size()]), instance);
        System.out.println("Solution OF : " + solution.getOF() + " with " + solution.countFilledRoute() + " routes " + solution.countAllAppointment() + " appointments / " + instance.getAppointmentManager().getAppointments().size());
        if (checker.checkSolution()) {
            System.out.println("Success ! :)");
        } else {
            System.err.println("Failure ! :(");
        }

        Solution backup = solution.clone();

        System.out.println("Running LNS");
        LargeNeighborhoodSearch lns = new LargeNeighborhoodSearch();
        boolean success = true;
        solution = lns.run(properties, solution);

        checker = new Checker(solution.getRoutes().toArray(new AdvancedRoute[solution.getRoutes().size()]), instance);
        if (checker.checkSolution()) {
            System.out.println("Success ! :)");
        } else {
            System.err.println("Failure ! :( Retry once");
            solution = lns.run(properties, backup);
            checker = new Checker(solution.getRoutes().toArray(new AdvancedRoute[solution.getRoutes().size()]), instance);
            if (checker.checkSolution()) {
                System.out.println("Success ! :)");
            } else {
                System.err.println("Failure ! :( again");
                success = false;
            }
        }

        if (success) {
            System.out.println("LNS -> Solution OF : " + solution.getOF() + " with " + solution.countFilledRoute() + " routes " + solution.countAllAppointment() + " appointments / " + instance.getAppointmentManager().getAppointments().size());
        }

        SolutionWriter solutionWriter = new SolutionWriter();
        solutionWriter.writeSolution(solution, filename + "_solution.json");

        return solution;
    }

    static public void main(String[] argv) {
        File folder = new File("instances/");
        File[] listOfFiles = folder.listFiles();


        for (File file : listOfFiles) {
            if (file.isFile() && file.getName().endsWith(".rl")) {
                String instanceName = file.getName().substring(0, file.getName().length() - 3);
                System.out.println("Loading " + instanceName);

                InstanceDistanceMatrix instance = loadInstance("instances/" + instanceName);
                Solution solution = runAlgorithm(instance, "instances/" + instanceName);
            }
        }

    }

    private static DateTime getMidnightTuesday() {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_WEEK, 1);//jump to tomorrow
        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.TUESDAY) {
            calendar.add(Calendar.DAY_OF_WEEK, 1);
        }
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return new DateTime(calendar.getTime());
    }
}
