package fr.univ_tours.polytech.di4.project.starter;

import fr.univ_tours.polytech.di4.project.algorithm.Algorithm;
import fr.univ_tours.polytech.di4.project.algorithm.SequentialAlgorithm;
import fr.univ_tours.polytech.di4.project.algorithm.lns.LargeNeighborhoodSearch;
import fr.univ_tours.polytech.di4.project.algorithm.solution.AdvancedRoute;
import fr.univ_tours.polytech.di4.project.algorithm.solution.Solution;
import fr.univ_tours.polytech.di4.project.checker.Checker;
import fr.univ_tours.polytech.di4.project.data.instance.InstanceDistanceMatrix;
import fr.univ_tours.polytech.di4.project.io.IOInstanceDistanceMatrix;
import fr.univ_tours.polytech.di4.project.io.solution.SolutionWriter;

import java.util.Properties;

/**
 * @author Nibeaudeau Timothy
 * @version 0.1
 *          This class is use to run the algorithm on data you can use a source file which mean the app will create a json file to store the result of the request
 *          on GoogleMap @see {@link IOInstanceDistanceMatrix}, {@link GetResources}
 *          or this class will load the data directly from json fil which already contain all value necessary.
 */
public class OpTurnGoogle {
    private static InstanceDistanceMatrix instanceDistanceMatrix = null;

    /**
     * Main
     *
     * @param args first param need to be rl file or json, , second param (optional) filename of the backup file
     */
    public static void main(String[] args) {
        Properties properties = LoadProperties.getProperties();
        if (!properties.isEmpty()) {
            String filename = args[0];
            if (args.length <= 0 || (!filename.contains(".rl") && !filename.contains(".json"))) {
                throw new IllegalArgumentException("You need to give source file (.rl) or the matrix file (.json)");
            }

            if (filename.contains(".rl")) {
                //create an instance
                GetResources.main(args);
                instanceDistanceMatrix = GetResources.getInstanceDistanceMatrix();
            } else {
                instanceDistanceMatrix = IOInstanceDistanceMatrix.readJson(filename);
            }

            Algorithm algorithm = new SequentialAlgorithm(instanceDistanceMatrix);
            Solution solution = algorithm.run(properties);
            LargeNeighborhoodSearch lns = new LargeNeighborhoodSearch();
            solution = lns.run(properties, solution);
            System.out.println("SOLUTION COST: " + solution.getOF());

            SolutionWriter solutionWriter = new SolutionWriter();
            solutionWriter.writeSolution(solution, "solution.json");
            Checker checker = new Checker(solution.getRoutes().toArray(new AdvancedRoute[solution.getRoutes().size()]), instanceDistanceMatrix);
            if (checker.checkSolution()) {
                System.out.println("Success ! :)");
            } else {
                System.out.println("Failure ! :(");
            }
        }
    }
}
